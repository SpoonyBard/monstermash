package enums;

/**
 * Enum representing the armor types in the DnD universe.
 */
public enum ArmorType {
    LIGHT,
    MEDIUM,
    HEAVY;

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
