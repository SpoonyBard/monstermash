package enums;

/**
 * This enum provides data on the number of damage dice, the type of damage die,
 * and the type of damage of all manufactured weapons except the net,
 * which has no damage dice.
 */
public enum Weapon {
    CLUB(1, DieType.D4, DamageType.BLUDGEONING, 5, 0, 0, false),
    DAGGER(1, DieType.D4, DamageType.PIERCING, 5, 20, 60, true),
    GREATCLUB(1, DieType.D8, DamageType.BLUDGEONING, 5, 0, 0, false),
    HANDAXE(1, DieType.D6, DamageType.SLASHING, 5, 20, 60, false),
    JAVELIN(1, DieType.D6, DamageType.PIERCING, 5, 30, 120, false),
    LIGHT_HAMMER(1, DieType.D4, DamageType.BLUDGEONING, 5, 20, 60, false),
    MACE(1, DieType.D6, DamageType.BLUDGEONING, 5, 0, 0, false),
    QUARTERSTAFF(1, DieType.D6, DamageType.BLUDGEONING, 5, 0, 0, false),
    SICKLE(1, DieType.D4, DamageType.SLASHING, 5, 0, 0, false),
    SPEAR(1, DieType.D6, DamageType.PIERCING, 5, 20, 60, false),

    LIGHT_CROSSBOW(1, DieType.D8, DamageType.PIERCING, 0, 80, 320, false),
    DART(1, DieType.D8, DamageType.PIERCING, 0, 20, 60, false),
    SHORTBOW(1, DieType.D6, DamageType.PIERCING, 0, 80, 320, false),
    SLING(1, DieType.D4, DamageType.BLUDGEONING, 0, 30, 120, false),

    BATTLEAXE(1, DieType.D8, DamageType.SLASHING, 5, 0, 0, false),
    FLAIL(1, DieType.D8, DamageType.BLUDGEONING, 5, 0, 0, false),
    GLAIVE(1, DieType.D10, DamageType.SLASHING, 10, 0, 0, false),
    GREATAXE(1, DieType.D12, DamageType.SLASHING, 5, 0, 0, false),
    GREATSWORD(2, DieType.D6, DamageType.SLASHING, 5, 0, 0, false),
    HALBERD(1, DieType.D10, DamageType.SLASHING, 10, 0, 0, false),
    LANCE(1, DieType.D12, DamageType.PIERCING, 10, 0, 0, false),
    LONGSWORD(1, DieType.D8, DamageType.SLASHING, 5, 0, 0, false),
    MAUL(2, DieType.D6, DamageType.BLUDGEONING, 5, 0, 0, false),
    MORNINGSTAR(1, DieType.D8, DamageType.PIERCING, 5, 0, 0, false),
    PIKE(1, DieType.D10, DamageType.PIERCING, 10, 0, 0, false),
    RAPIER(1, DieType.D8, DamageType.PIERCING, 5, 0, 0, true),
    SCIMITAR(1, DieType.D6, DamageType.SLASHING, 5, 0, 0, true),
    SHORTSWORD(1, DieType.D6, DamageType.PIERCING, 5, 0, 0, true),
    TRIDENT(1, DieType.D6, DamageType.PIERCING, 5, 20, 60, false),
    WAR_PICK(1, DieType.D8, DamageType.PIERCING, 5, 0, 0, false),
    WARHAMMER(1, DieType.D8, DamageType.PIERCING, 5, 0, 0, false),
    WHIP(1, DieType.D4, DamageType.SLASHING, 10, 0, 0, true),

    BLOWGUN(1, DieType.D1, DamageType.PIERCING, 0, 25, 100, false),
    HAND_CROSSBOW(1, DieType.D6, DamageType.PIERCING, 0, 30, 120, false),
    HEAVY_CROSSBOW(1, DieType.D10, DamageType.PIERCING, 0, 100, 400, false),
    LONGBOW(1, DieType.D8, DamageType.PIERCING, 0, 150, 600, false);

    private final int numDice;
    private final DieType damageDie;
    private final DamageType damageType;
    private final int reach;
    private final int range;
    private final int longrange;
    private final boolean finesse;

    Weapon(int numDice, DieType damageDie, DamageType damageType, int reach, int range, int longrange, boolean finesse) {
        this.numDice = numDice;
        this.damageDie = damageDie;
        this.damageType = damageType;
        this.reach = reach;
        this.range = range;
        this.longrange = longrange;
        this.finesse = finesse;
    }

    public int numDice() {
        return numDice;
    }

    public DieType damageDie() {
        return damageDie;
    }

    public DamageType damageType() {
        return damageType;
    }

    public int[] ranges() {
        int[] ranges = new int[3];
        ranges[0] = reach;
        ranges[1] = range;
        ranges[2] = longrange;
        return ranges;
    }

    public String toString() {
        String[] split = this.name().split("_");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].substring(0, 1) + split[i].substring(1).toLowerCase();
        }
        return String.join(" ", split) + " (" + numDice + damageDie + " " + damageType.toString().toLowerCase() + " damage)";
    }

    public String getStringName() {
        String[] split = this.name().split("_");
        for (int i=0; i < split.length; i++) {
            split[i] = split[i].substring(0,1) + split[i].substring(1).toLowerCase();
        }
        return String.join(" ", split);
    }

    public boolean isFinesse() {
        return finesse;
    }
}
