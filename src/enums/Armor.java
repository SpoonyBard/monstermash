package enums;

/**
 * Enum representing the types of manufactured armor in the DnD universe.
 * Armor has a name, a base armor class that it confers, and an armor type
 * which determines what other modifiers to add to armor class.
 */
public enum Armor {
    NONE(10, ArmorType.LIGHT),
    PADDED(11, ArmorType.LIGHT),
    LEATHER(11, ArmorType.LIGHT),
    STUDDED_LEATHER(12, ArmorType.LIGHT),

    HIDE(12, ArmorType.MEDIUM),
    CHAIN_SHIRT(13, ArmorType.MEDIUM),
    SCALE_MAIL(14, ArmorType.MEDIUM),
    BREASTPLATE(14, ArmorType.MEDIUM),
    HALF_PLATE(15, ArmorType.MEDIUM),

    RING_MAIL(14, ArmorType.HEAVY),
    CHAIN_MAIL(16, ArmorType.HEAVY),
    SPLINT(17, ArmorType.HEAVY),
    PLATE(18, ArmorType.HEAVY);

    private final int baseAC;
    private final ArmorType armorType;

    Armor(int baseAC, ArmorType armorType) {
        this.baseAC = baseAC;
        this.armorType = armorType;
    }

    public int baseAC() {
        return baseAC;
    }

    public ArmorType armorType() {
        return armorType;
    }

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase().replace("_", " ");
    }
}
