package enums;

/**
 * Enum representing the range of Challenge Ratings in DnD.
 * Challenge Rating is an average of the Offensive and Defensive
 * Ratings. Each Challenge Rating has an associated Experience Point
 * value, display string, and double value for calculations of averages.
 *
 * The other associated values help calculate the offensive and defensive ratings
 * for a given monster. They include the ranking of the CRs (to help with math involving
 * fractional CRs), the proficiency bonus associated with them, their associated hit points (HP),
 * their attack bonus (AB), their damage per round (DPR), and their save difficulty class (DC).
 * TODO: add a description of all the new data
 */
public enum ChallengeRating {
    ZERO("0", "0", 1, 0, 2, 13, 6, 3, 1, 13),
    EIGHTH("25", "1/8", 2, 0.125, 2, 13, 35, 3, 3, 13),
    FOURTH("50", "1/4", 3, 0.25, 2, 13, 49, 3, 5, 13),
    HALF("100", "1/2", 4, 0.5, 2, 13, 70, 3, 8, 13),
    ONE("200", "1", 5, 1, 2, 13, 85, 3, 14, 13),
    TWO("450", "2", 6, 2, 2, 13, 100, 3, 20, 13),
    THREE("700", "3", 7, 3, 2, 13, 115, 4, 26, 13),
    FOUR("1,100", "4", 8, 4, 2, 14, 130, 5, 32, 14),
    FIVE("1,800", "5", 9, 5, 3, 15, 145, 6, 38, 15),
    SIX("2,300", "6", 10, 6, 3, 15, 160, 6, 44, 15),
    SEVEN("2,900", "7", 11, 7, 3, 15, 175, 6, 50, 15),
    EIGHT("3,900", "8", 12, 8, 3, 16, 190, 7, 56, 16),
    NINE("5,000", "9", 13, 9, 4, 16, 205, 7, 62, 16),
    TEN("5,900", "10", 14, 10, 4, 17, 220, 7, 68, 16),
    ELEVEN("7,200", "11", 15, 10, 4, 17, 325, 8, 74, 17),
    TWELVE("8,400", "12", 16, 12, 4, 17, 250, 8, 80, 17),
    THIRTEEN("10,000", "13", 17, 13, 5, 18, 265, 8, 86, 18),
    FOURTEEN("11,500", "14", 18, 14, 5, 18, 280, 8, 92, 18),
    FIFTEEN("13,000", "15", 19, 15, 5, 18, 295, 8, 98, 18),
    SIXTEEN("15,000", "16", 20, 16, 5, 18, 310, 9, 104, 18),
    SEVENTEEN("18,000", "17", 21, 17, 6, 19, 325, 10, 110, 19),
    EIGHTEEN("20,000", "18", 22, 18, 6, 19, 340, 10, 116, 19),
    NINETEEN("22,000", "19", 23, 19, 6, 19, 355, 10, 122, 19),
    TWENTY("25,000", "20", 24, 20, 6, 19, 400, 10, 140, 19),
    TWENTYONE("33,000", "21", 25, 21, 7, 19, 445, 11, 158, 20),
    TWENTYTWO("41,000", "22", 26, 22, 7, 19, 490, 11, 176, 20),
    TWENTYTHREE("50,000", "23", 27, 23, 7, 19, 535, 11, 194, 20),
    TWENTYFOUR("62,000", "24", 28, 24, 7, 19, 580, 12, 212, 21),
    TWENTYFIVE("75,000", "25", 29, 25, 8, 19, 625, 12, 230, 21),
    TWENTYSIX("90,000", "26", 30, 26, 8, 19, 670, 12, 248, 21),
    TWENTYSEVEN("105,000", "27", 31, 27, 8, 19, 715, 13, 266, 22),
    TWENTYEIGHT("120,000", "28", 32, 28, 8, 19, 760, 13, 284, 22),
    TWENTYNINE("135,000", "29", 33, 29, 9, 19, 805, 13, 302, 22),
    THIRTY("155,000", "30", 34, 30, 9, 19, 850, 14, 320, 23);

    private final String XP;
    private final String CRdisplay;
    private final int CRranking;
    private final double CRvalue;
    private final int proficiencyBonus;
    private final int AC;
    private final int HP;
    private final int AB;
    private final int DPR;
    private final int DC;

    ChallengeRating(String XP, String CRdisplay, int CRranking, double CRvalue, int proficiencyBonus, int AC, int HP, int AB, int DPR, int DC) {
        this.XP = XP;
        this.CRdisplay = CRdisplay;
        this.CRranking = CRranking;
        this.CRvalue = CRvalue;
        this.proficiencyBonus = proficiencyBonus;
        this.AC = AC;
        this.HP = HP;
        this.AB = AB;
        this.DPR = DPR;
        this.DC = DC;
    }

    public String XP() {return XP;}
    public String CRdisplay() {return CRdisplay;}
    public int CRranking() {return CRranking;}
    public double CRvalue() {return CRvalue;}
    public int proficiencyBonus() {return proficiencyBonus;}
    public int AC() {return AC;}
    public int HP() {return HP;}
    public int attackBonus() {return AB;}
    public int DPR() {return DPR;}
    public int DC() {return DC;}

    public String toString() {
        return CRdisplay;
    }
}
