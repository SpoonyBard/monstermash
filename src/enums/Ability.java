package enums;

/**
 * Enum representing the 6 ability scores of DnD.
 */
public enum Ability {
    STR,
    DEX,
    CON,
    INT,
    WIS,
    CHA;

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
