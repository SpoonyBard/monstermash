package enums;

/**
 * Enum representing the possible sizes of monster in the DnD universe.
 * Size incorporates the multiplier for damage dice associated with that
 * size, as well as the polyhedral die type rolled to determine hitDice.
 */

public enum Size {
    TINY(1, DieType.D4),
    SMALL(1, DieType.D6),
    MEDIUM(1, DieType.D8),
    LARGE(2, DieType.D10),
    HUGE(3, DieType.D12),
    GARGANTUAN(4, DieType.D20);

    private final int multiplier;
    private final DieType hitDie;

    Size(int multiplier, DieType hitDie) {
        this.multiplier = multiplier;
        this.hitDie = hitDie;
    }

    public int multiplier() {return multiplier;}
    public DieType hitDie() {return hitDie;}

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
