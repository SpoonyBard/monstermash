package enums;

/**
 * Enum representing the types of special senses in the DnD universe.
 */
public enum Sense {
    DARKVISION,
    BLINDSIGHT,
    TRUESIGHT,
    TREMORSENSE;

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
