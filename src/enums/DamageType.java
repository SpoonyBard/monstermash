package enums;

/**
 * Enum representing the damage types in the DnD universe.
 */
public enum DamageType {
    BLUDGEONING,
    PIERCING,
    SLASHING,
    ACID,
    COLD,
    FIRE,
    FORCE,
    LIGHTNING,
    NECROTIC,
    POISON,
    PSYCHIC,
    RADIANT,
    THUNDER;

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
