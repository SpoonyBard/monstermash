package enums;

/**
 * Enum representing the types of polyhedral dice used in DnD.
 * Each die has an associated average roll for determining expected
 * value of a series of rolls.
 */
public enum DieType {
    D1(1),
    D4(2.5),
    D6(3.5),
    D8(4.5),
    D10(5.5),
    D12(6.5),
    D20(10.5);

    private final double avgRoll;

    DieType(double avgRoll) {
        this.avgRoll = avgRoll;
    }

    public double avgRoll() {return avgRoll;}

    public String toString() {
        if (this == DieType.D1) {
            return "";
        }
        else {
            return this.name().toLowerCase();
        }
    }
}
