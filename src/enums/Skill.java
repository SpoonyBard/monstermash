package enums;

import enums.Ability;

/**
 * Enum representing the skills in the DnD universe as well as
 * their associated ability.
 */
public enum Skill {
    ACROBATICS(Ability.DEX),
    ANIMAL_HANDLING(Ability.WIS),
    ARCANA(Ability.INT),
    ATHLETICS(Ability.STR),
    DECEPTION(Ability.CHA),
    HISTORY(Ability.INT),
    INSIGHT(Ability.WIS),
    INTIMIDATION(Ability.CHA),
    INVESTIGATION(Ability.INT),
    MEDICINE(Ability.WIS),
    NATURE(Ability.INT),
    PERCEPTION(Ability.WIS),
    PERFORMANCE(Ability.CHA),
    PERSUASION(Ability.CHA),
    RELIGION(Ability.INT),
    SLEIGHT_OF_HAND(Ability.DEX),
    STEALTH(Ability.DEX),
    SURVIVAL(Ability.WIS);

    private final Ability associatedAbility;

    Skill(Ability associatedAbility) {
        this.associatedAbility = associatedAbility;
    }

    public String toString() {
        String[] split = this.name().split("_");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].substring(0, 1) + split[i].substring(1).toLowerCase();
        }
        return String.join(" ", split);
    }

    public Ability getAssociatedAbility() {
        return associatedAbility;
    }
}
