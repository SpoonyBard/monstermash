package enums;

/**
 * Enum representing the types of dice code used for attacks in DnD.
 */
public enum AttackDiceCode {
    ONED4(1, DieType.D4),
    ONED6(1, DieType.D6),
    ONED8(1, DieType.D8),
    ONED10(1, DieType.D10),
    ONED12(1, DieType.D12),
    TWOD6(2, DieType.D6);

    private final int numDice;
    private final DieType dieType;

    AttackDiceCode(int numDice, DieType dieType) {
        this.numDice = numDice;
        this.dieType = dieType;
    }

    public int getNumDice() {return numDice;}
    public DieType getDieType() {return dieType;}
    public String toString() {
        return String.valueOf(numDice) + dieType;
    }
}
