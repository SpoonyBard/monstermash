package enums;

/**
 * Enum representing the types of special movement in the DnD universe.
 */
public enum Speed {
    BURROW,
    CLIMB,
    FLY,
    SWIM;

    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();
    }
}
