import controller.*;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Monster;

public class MonsterMash extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ControllerInterface controller = new StatsController(primaryStage);
    }
}
