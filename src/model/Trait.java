package model;

/**
 * Class representing a passive trait belonging to the monster
 */
public class Trait implements Comparable<Trait> {
    private int damagePerRound;
    private double hitPointMultiplier;
    private int bonusToHit;
    private int bonusToArmorClass;
    private String description;
    private String name;

    public Trait(String name, String description, int bonusToArmorClass, int bonusToHit, double hitPointMultiplier, int damagePerRound) {
        this.name = name;
        this.description = description;
        this.bonusToArmorClass = bonusToArmorClass;
        this.bonusToHit = bonusToHit;
        this.hitPointMultiplier = hitPointMultiplier;
        this.damagePerRound = damagePerRound;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getBonusToArmorClass() {return bonusToArmorClass;}
    public int getBonusToHit() {return bonusToHit;}
    public double getHitPointMultiplier() {return hitPointMultiplier;}
    public int getDPR() {return damagePerRound;}

    public int compareTo(Trait trait) {
        return name.compareTo(trait.getName());
    }
}
