package model;

/**
 * Class representing an action of the monster
 */
public class Action implements Comparable<Action> {
    private String description;
    private String name;

    public Action(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int compareTo(Action action) {
        return name.compareTo(action.getName());
    }
}

