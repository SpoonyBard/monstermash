package model;

import enums.*;
/**
 * Class representing attacks, either using a manufactured weapon or
 * custom built by the user
 */
public class Attack implements Comparable<Attack> {
    private final Monster monster;
    private String name;
    private String description;
    private Weapon manufacturedWeapon;
    private Ability ability;
    private int numDice;
    private DieType damageDie;
    private DamageType damageType;
    private Rider rider = null;
    private boolean custom;
    private int[] ranges = new int[3];

    // Constructor for weapon template, no rider
    Attack (Monster monster, String name, Weapon manufacturedWeapon, String description) {
        this.monster = monster;
        this.name = name;
        this.description = description;
        this.manufacturedWeapon = manufacturedWeapon;
        this.ranges = manufacturedWeapon.ranges();
        this.custom = false;
        this.numDice = monster.getSizeMultiplier() * manufacturedWeapon.numDice();
        this.damageDie = manufacturedWeapon.damageDie();
        this.damageType = manufacturedWeapon.damageType();
        if (ranges[0] == 0) {
            this.ability = Ability.DEX;
        } else if (manufacturedWeapon.isFinesse() && (monster.getAbilityScore(Ability.DEX) > monster.getAbilityScore(Ability.STR))) {
            this.ability = Ability.DEX;
        } else {
            this.ability = Ability.STR;
        }
    }

    // Constructor for weapon template and rider
    public Attack (Monster monster, String name, Weapon manufacturedWeapon, Rider rider, String description){
        this.monster = monster;
        this.name = name;
        this.description = description;
        this.manufacturedWeapon = manufacturedWeapon;
        this.ranges = manufacturedWeapon.ranges();
        this.custom = false;
        this.numDice = monster.getSizeMultiplier() * manufacturedWeapon.numDice();
        this.damageDie = manufacturedWeapon.damageDie();
        this.damageType = manufacturedWeapon.damageType();
        if (ranges[0] == 0) {
            this.ability = Ability.DEX;
        } else if (manufacturedWeapon.isFinesse() && (monster.getAbilityScore(Ability.DEX) > monster.getAbilityScore(Ability.STR))) {
            this.ability = Ability.DEX;
        } else {
            this.ability = Ability.STR;
        }
        this.rider = rider;
    }

    // Constructor for custom template without rider
    Attack(Monster monster, String name, Ability ability, int numDice, DieType damageDie, DamageType damageType, int reach, int range, int longrange, String description) {
        this.monster = monster;
        this.name = name;
        this.ability = ability;
        this.numDice = numDice;
        this.damageDie = damageDie;
        this.damageType = damageType;
        this.ranges[0] = reach;
        this.ranges[1] = range;
        this.ranges[2] = longrange;
        this.description = description;
        this.custom = true;
    }

    // Constructor for custom template with rider
    public Attack (Monster monster, String name, Ability ability, int numDice, DieType damageDie, DamageType damageType, int reach, int range, int longrange, Rider rider, String description) {
        this.monster = monster;
        this.name = name;
        this.ability = ability;
        this.numDice = numDice;
        this.damageDie = damageDie;
        this.damageType = damageType;
        this.ranges[0] = reach;
        this.ranges[1] = range;
        this.ranges[2] = longrange;
        this.rider = rider;
        this.description = description;
        this.custom = true;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Weapon getWeapon() {
        if(!custom) {
            return manufacturedWeapon;
        } else {
            return null;
        }
    }

    // method that allows the attack's original dice code information to be repopulated in the form
    public AttackDiceCode getCustomAttackDiceCode() {
        for (AttackDiceCode a : AttackDiceCode.values()) {
            if (a.getNumDice() == numDice) {
                if (a.getDieType() == damageDie) {
                    return a;
                }
            }
        }
        return null;
    }
    public Ability getAbility() {
        return ability;
    }

    public boolean getCustom() {
        return custom;
    }

    public int compareTo(Attack attack) {
        return name.compareTo(attack.getName());
    }

    public int getAbilityMod() {
        return monster.getModifier(ability);
    }

    private int getProfBonus() {
        return monster.getProficiencyBonus();
    }

    public int getDPR() {
        int diceRolled;
        if (custom){
            diceRolled = monster.getSizeMultiplier() * numDice;
        } else {
            diceRolled = monster.getSizeMultiplier() * manufacturedWeapon.numDice();
        }
        double dieDamage = damageDie.avgRoll() * diceRolled;
        return (int)Math.ceil(dieDamage) + getAbilityMod();
    }

    public int getToHitBonus() {
        return getAbilityMod() + getProfBonus();
    }

    public String getRangeString() {
        if (ranges[0] == 0) {
            return "Ranged";
        } else if ((ranges[1] == 0) && (ranges[2] == 0)) {
            return "Melee";
        } else {return "Melee or Ranged";}
    }

    public String getDiceCode() {
        int diceRolled;
        if (custom) {
            diceRolled = monster.getSizeMultiplier() * numDice;
        } else {
            diceRolled = monster.getSizeMultiplier() * manufacturedWeapon.numDice();
        }
        return String.valueOf(diceRolled) + String.valueOf(damageDie);
    }


    public DamageType getDamageType() {
        return damageType;
    }

    public int getReach() {
        return ranges[0];
    }

    public int getShortRange() {
        return ranges[1];
    }

    public int getLongRange() {
        return ranges[2];
    }

    public boolean isRanged() {
        return ((ranges[1] != 0) && (ranges[2] != 0));
    }

    public boolean isMelee() {
        return (ranges[0] != 0);
    }
}