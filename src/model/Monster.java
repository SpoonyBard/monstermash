package model;

import enums.*;

import java.util.*;

public class Monster implements MonsterInterface {
    /*
     * Part 1: Instance Variables
     */
    private String name = "";
    private ChallengeRating CR = ChallengeRating.ONE;
    private Alignment alignment;
    private MonsterType type;
    private String subtype = "";
    private Size size;
    private int speed = 30;
    private String languages = "";
    private Map<Ability, Integer> abilityScores = new HashMap<>();
    private Map<Sense, Integer> senses = new HashMap<>();
    private Map<Speed, Integer> specialSpeeds = new HashMap<>();
    private List<Skill> skills = new ArrayList<>();
    private Armor armor;
    private boolean shield = false;
    private int natArmorBonus = 0;
    private int numHitDice = 1;
    private List<DamageType> immunities = new ArrayList<>();
    private List<DamageType> resistances = new ArrayList<>();
    private List<DamageType> vulnerabilities = new ArrayList<>();
    private List<ConditionType> conditions = new ArrayList<>();
    private List<Ability> saves = new ArrayList<>();
    private Map<String, Trait> traits = new HashMap<>();
    private Map<String, Action> actions = new HashMap<>();
    private Map<String, Attack> attacks = new HashMap<>();


    /*
     * Part 2: Constructor
     */
    public Monster() {
        name = "[name]";
        size = Size.MEDIUM;
        armor = Armor.NONE;
        abilityScores.put(Ability.STR, 10);
        abilityScores.put(Ability.DEX, 10);
        abilityScores.put(Ability.CON, 10);
        abilityScores.put(Ability.INT, 10);
        abilityScores.put(Ability.WIS, 10);
        abilityScores.put(Ability.CHA, 10);
        addAttack("Example Attack", Weapon.LONGSWORD, "");
    }

    /*
     * Part 3.1:  Setters and Getters for Fluff
     */
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setAlignment(Alignment alignment){
        this.alignment = alignment;
    }
    public Alignment getAlignment() {
        return alignment;
    }

    public void setType(MonsterType monsterType){
        type = monsterType;
    }
    public MonsterType getType() {
        return type;
    }
    public void setSubtype(String subtype){
        this.subtype = subtype;
    }
    public String getSubtype() {
        return subtype;
    }

    public void setLanguageList(String languageList){
        languages = languageList;
    }
    public String getLanguageList() {
        return languages;
    }

    public void addSense(Sense sense, int range){
        senses.put(sense, range);
    }
    public void removeSense(Sense sense){
        senses.remove(sense);
    }
    public Map<Sense, Integer> getSenses(){
        return senses;
    }

    public void addSkill(Skill skill){
        skills.add(skill);
        Collections.sort(skills);
    }
    public void removeSkill(Skill skill){
        skills.remove(skill);
    }
    public List<Skill> getSkills(){
        return skills;
    }
    public int getSkillModifier(Skill skill) {
        Ability ability = skill.getAssociatedAbility();
        if(skills.contains(skill)) {
            return getModifier(ability) + getProficiencyBonus();
        } else {
            return getModifier(ability);
        }
    }

    /*
     * Part 3.2: Methods for Core Data
     */

    /**
     * Sets a desired Challenge Rating for the monster.
     * @param CR
     */
    public void setCR(ChallengeRating CR) {
        this.CR = CR;
    }
    public ChallengeRating getCR() {
        return CR;
    }
    public int getProficiencyBonus() { return CR.proficiencyBonus();}
    public int getSizeMultiplier() {
        return size.multiplier();
    }

    /**
     * Calculates the monster's current challenge rating based on its stats
     * @return
     */
    public ChallengeRating getCurrentCR() {
        ChallengeRating DR = getCurrentDR();
        ChallengeRating OR = getCurrentOR();
        double CR = (DR.CRvalue() + OR.CRvalue()) / 2;
        ChallengeRating currentCR = ChallengeRating.ZERO;
        for (ChallengeRating c : ChallengeRating.values()) {
            double curVal = currentCR.CRvalue();
            double cVal = c.CRvalue();
            if (Math.abs(CR-cVal) < Math.abs(CR-curVal)) {
                currentCR = c;
            }
        }
        return currentCR;
    }

    public void setSize(Size size){
        this.size = size;
    }
    public Size getSize() {
        return size;
    }

    public void setAbilityScore(Ability ability, int newScore) {
        abilityScores.put(ability, newScore);
    }
    public int getAbilityScore(Ability ability) {
        return abilityScores.get(ability);
    }
    public int getModifier(Ability ability) {
        int score = abilityScores.get(ability);
        return score / 2 - 5;
    }

    /**
     * Gives a monster proficiency in saves of a certain ability
     * @param saveAbility
     */
    public void addSave(Ability saveAbility) {
        saves.add(saveAbility);
        Collections.sort(saves);
    }
    public void removeSave(Ability saveAbility) {
        saves.remove(saveAbility);
    }
    public List<Ability> getSaves() {
        return saves;
    }
    public int getSaveModifier(Ability saveAbility) {
        if (saves.contains(saveAbility)) {
            return getModifier(saveAbility) + getProficiencyBonus();
        } else {
            return getModifier(saveAbility);
        }
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }
    public int getSpeed() {
        return speed;
    }

    /**
     * Adds special speeds such as flight and burrowing
     * @param speedType
     * @param speed
     */
    public void addSpecialSpeed(Speed speedType, int speed) {
        specialSpeeds.put(speedType, speed);
    }
    public void removeSpecialSpeed(Speed speedType) {
        specialSpeeds.remove(speedType);
    }
    public Map<Speed, Integer> getSpecialSpeeds() {
        return specialSpeeds;
    }

    /*
     * Part 3.3: Methods for Defense
     */

    /**
     * Calculates a monster's defensive challenge rating based on its stats
     * @return
     */
    public ChallengeRating getCurrentDR() {
        ChallengeRating hitPointRating = getHitPointRating();
        int expectedAC = hitPointRating.AC();
        int effectiveAC = getEffectiveAC();
        int adjustDR = (effectiveAC - expectedAC)/2;
        return adjustCR(hitPointRating, adjustDR);
    }

    /**
     * Returns a challenge rating that is shift levels higher or lower than a given CR
     * @param CR
     * @param shift
     * @return
     */
    private ChallengeRating adjustCR(ChallengeRating CR, int shift) {
        int CRrank = CR.CRranking();
        int newCRrank = CRrank + shift;
        ChallengeRating finalCR = CR;
        if (newCRrank <= 0) {
            finalCR = ChallengeRating.ZERO;
        } else {
            for (ChallengeRating c : ChallengeRating.values()) {
                if (c.CRranking() == newCRrank) {
                    finalCR = c;
                }
            }
        }
        return finalCR;
    }

    /**
     * Finds the CR corresponding to the monster's effective hit points
     * @return
     */
    private ChallengeRating getHitPointRating() {
        int HP = getEffectiveHP();
        ChallengeRating HPrating = ChallengeRating.THIRTY;
        for (ChallengeRating c : ChallengeRating.values()) {
            if (c.HP() > HP) {
                HPrating = c;
                break;
            }
        }
        return HPrating;
    }

    public void setNumHitDice(int numHitDice) {
        this.numHitDice = numHitDice;
    }
    public int getNumHitDice() {
        return numHitDice;
    }
    public DieType getHitDieType() {
        return size.hitDie();
    }

    /**
     * Finds the number of hit points from the monster's Constitution modifier
     * @return
     */
    public int getHitPointsFromCon(){
        return getModifier(Ability.CON) * numHitDice;
    }

    /**
     * Finds a monster's total health
     * @return
     */
    public int getHitPoints(){
        DieType dieType = getHitDieType();
        double hpFromDice = dieType.avgRoll() * numHitDice;
        int hp = (int) hpFromDice + getHitPointsFromCon();
        if (hp > 0) {
            return hp;
        } else {
            return 1;
        }
    }

    /**
     * Finds a monster's adjusted health after taking traits into account
     * @return
     */
    private int getEffectiveHP() {
        double HP = getHitPoints();
        for (Trait f : traits.values()) {
            HP = HP * f.getHitPointMultiplier();
        }
        HP = HP * physicalResilienceMultiplier();
        return (int)HP;
    }

    /**
     * Adds an HP multiplier based on a monster's resistances
     * @return
     */
    private double physicalResilienceMultiplier() {
        double RIMultiplier = 1;
        boolean resistPhysical = false;
        if (resistances.contains(DamageType.BLUDGEONING) && resistances.contains(DamageType.PIERCING) && resistances.contains(DamageType.SLASHING)) {
            resistPhysical = true;
        }
        boolean immuneToPhysical = false;
        if (immunities.contains(DamageType.BLUDGEONING) && immunities.contains(DamageType.PIERCING) && immunities.contains(DamageType.SLASHING)) {
            immuneToPhysical = true;
        }
        double targetCR = CR.CRvalue();
        if (targetCR <= 4) {
            if (immuneToPhysical || resistPhysical) {
                RIMultiplier = 2;
            }
        } else if (targetCR <= 10) {
            if (immuneToPhysical) {
                RIMultiplier = 2;
            } else if (resistPhysical) {
                RIMultiplier = 1.5;
            }
        } else if (targetCR <= 16) {
            if (immuneToPhysical) {
                RIMultiplier = 1.5;
            } else if (resistPhysical) {
                RIMultiplier = 1.25;
            }
        } else {
            if (immuneToPhysical) {
                RIMultiplier = 1.25;
            }
        }
        return RIMultiplier;
    }

    /**
     * Finds a monster's total AC
     * @return
     */
    public int getAC(){
        int AC;
        int acFromArmor = getACFromArmor();
        int acFromNatArmor = getACFromNatArmor();

        if (acFromArmor >= acFromNatArmor) {
            AC = acFromArmor;
        } else {
            AC = acFromNatArmor;
        }

        if (shield) {AC += 2;}

        return AC;
    }

    /**
     * Finds a monster's effective AC, adjusted for traits
     * @return
     */
    private int getEffectiveAC() {
        int effectiveAC = getAC();
        for (Trait f : traits.values()) {
            effectiveAC = effectiveAC + f.getBonusToArmorClass();
        }
        if (specialSpeeds.containsKey(Speed.FLY)) {
            effectiveAC += 2;
        }
        int numSaves = saves.size();
        if (numSaves >= 5) {
            effectiveAC += 4;
        } else if (numSaves >= 3) {
            effectiveAC += 2;
        }
        return effectiveAC;
    }

    public void setShield(boolean hasShield) {
        shield = hasShield;
    }

    public boolean hasShield() {
        return shield;
    }

    public void setNaturalArmor(int natArmorBonus) {
        this.natArmorBonus = natArmorBonus;
    }

    public int getNaturalArmor() {
        return natArmorBonus;
    }

    public int getACFromArmor() {
        int dexAC = 0;
        switch (armor.armorType()) {
            case LIGHT:
                dexAC = getModifier(Ability.DEX);
                break;
            case MEDIUM:
                dexAC = getModifier(Ability.DEX);
                if (dexAC > 2) {dexAC = 2;}
                break;
            case HEAVY:
                break;
        }
        return armor.baseAC() + dexAC;
    }

    public int getACFromNatArmor() {
        return 10 + natArmorBonus + getModifier(Ability.DEX);
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor){
        this.armor = armor;
    }

    public void addDamageImmunity(DamageType damageType){
        immunities.add(damageType);
        Collections.sort(immunities);
    }
    public void removeDamageImmunity(DamageType damageType){
        immunities.remove(damageType);
    }
    public List<DamageType> getDamageImmunities(){
        return immunities;
    }

    public void addDamageResistance(DamageType damageType){
        resistances.add(damageType);
        Collections.sort(resistances);
    }
    public void removeDamageResistance(DamageType damageType){
        resistances.remove(damageType);
    }
    public List<DamageType> getDamageResistances(){
        return resistances;
    }

    public void addDamageVulnerability(DamageType damageType) {
        vulnerabilities.add(damageType);
        Collections.sort(vulnerabilities);
    }
    public void removeDamageVulnerability(DamageType damageType) {vulnerabilities.remove(damageType);}
    public List<DamageType> getDamageVulnerabilities() {return vulnerabilities;}

    public void addConditionImmunity(ConditionType conditionType){
        conditions.add(conditionType);
        Collections.sort(conditions);
    }
    public void removeConditionImmunity(ConditionType conditionType){
        conditions.remove(conditionType);
    }
    public List<ConditionType> getConditionImmunities(){
        return conditions;
    }

    public void addAction(String name, String description) {
        Action newAction = new Action(name, description);
        actions.put(name, newAction);
    }

    /*
     * Part 3.4: Methods for Actions, Traits, and Offense
     */

    //Part 3.4.1: Calculating Offensive CR

    /**
     * Calculates a monster's offensive challenge rating based on its stats
     * @return
     */
    public ChallengeRating getCurrentOR() {
        ChallengeRating DR = findDamageRating();
        int AB = findAttackBonus();
        int expectedAB = DR.attackBonus();
        int adjustDR = (AB - expectedAB)/2;
        return adjustCR(DR, adjustDR);
    }

    /**
     * Finds the amount of damage the monster can deal in a round
     * @return
     */
    private int findDPR() {
        int DPR = 0;
        List<Attack> attacks = getAttacks();
        for (Attack curAttack : attacks) {
            int curDamage = curAttack.getDPR();
            if (curDamage > DPR) {
                DPR = curDamage;
            }
        }
        List<Trait> traits = getTraits();
        for (Trait curTrait : traits) {
            DPR += curTrait.getDPR();
        }
        return DPR;
    }

    /**
     * Matches a DPR to a challenge rating
     * @return
     */
    private ChallengeRating findDamageRating() {
        // Original method found closest DPR listed in CRs, but the values for DPR in the CR table really showed the
        // highest DPR of a range, so we want the first CR with a higher DPR than the monster.
        int DPR = findDPR();
        ChallengeRating damageRating = ChallengeRating.THIRTY;
        for (ChallengeRating c : ChallengeRating.values()) {
            if (c.DPR() >= DPR) {
                damageRating = c;
                break;
            }
        }
        return damageRating;
    }

    /**
     * Finds a monster's bonus to hit with attacks, used to calculate offensive CR
     * @return
     */
    private int findAttackBonus() {
        int AB = 0;
        List<Attack> attacks = getAttacks();
        int curBonus;
        for (Attack curAttack : attacks) {
            curBonus = curAttack.getToHitBonus();
            if (curBonus > AB) {
                AB = curBonus;
            }
        }
        List<Trait> traits = getTraits();
        for (Trait curTrait : traits) {
            AB += curTrait.getBonusToHit();
        }
        return AB;
    }

    // Part 3.4.2: Adding and removing attacks, actions, and traits

    public void removeAction(String name){
        if (actions.get(name) != null) {
            actions.remove(name);
        }
    }

    // Adder for attack with manufactured weapon and a rider
    public void addAttack(String name, Weapon manufacturedWeapon, int numRiderDice, DieType riderDie, DamageType riderType, String description){
        if (attacks.containsKey("Example Attack")) {
            attacks.remove("Example Attack");
        }
        Attack newAttack = new Attack(this, name, manufacturedWeapon, description);
        attacks.put(name, newAttack);
    }
    // Adder for attack with custom weapon
    public void addAttack(String name, Ability ability, int numDice, DieType damageDie, DamageType damageType, int reach, int range, int longrange, String description){
        if (attacks.containsKey("Example Attack")) {
            attacks.remove("Example Attack");
        }
        Attack newAttack = new Attack(this, name, ability, numDice, damageDie, damageType, reach, range, longrange, description);
        attacks.put(name, newAttack);
    }
    // Adder for attack with manufactured weapon, no rider
    public void addAttack(String name, Weapon manufacturedWeapon, String description) {
        if (attacks.containsKey("Example Attack")) {
            attacks.remove("Example Attack");
        }
        Attack newAttack = new Attack(this, name, manufacturedWeapon, description);
        attacks.put(name, newAttack);
    }

    public void removeAttack(String name) {
        if (attacks.get(name) != null) {
            attacks.remove(name);
        }
    }

    public List<Attack> getAttacks() {
        List<Attack> attackList = new ArrayList<>();
        for (Map.Entry<String, Attack> attacksByName : attacks.entrySet()) {
            attackList.add(attacksByName.getValue());
        }
        Collections.sort(attackList);
        return attackList;
    }

    public List<Action> getActions(){
        List<Action> actionList = new ArrayList<>();
        for (Map.Entry<String, Action> actionsByName : actions.entrySet()) {
            actionList.add(actionsByName.getValue());
        }
        Collections.sort(actionList);
        return actionList;
    }

    public void addTrait(String name, String description, int bonusToArmorClass, int bonusToHit, double hitPointMultiplier, int damagePer3Rounds) {
        Trait newTrait = new Trait(name, description, bonusToArmorClass, bonusToHit, hitPointMultiplier, damagePer3Rounds);
        traits.put(name, newTrait);
    }

    public void removeTrait(String name){
        if (traits.get(name) != null) {
            traits.remove(name);
        }
    }

    public List<Trait> getTraits(){
        List<Trait> traitList = new ArrayList<>();
        for (Map.Entry<String, Trait> traitsByName : traits.entrySet()) {
            traitList.add(traitsByName.getValue());
        }
        Collections.sort(traitList);
        return traitList;
    }
}