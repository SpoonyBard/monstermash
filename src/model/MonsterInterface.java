package model;

import enums.*;

import java.util.List;
import java.util.Map;

/**
 * Interface for the Monster class providing public facing methods
 * for the StatsController to use
 */
public interface MonsterInterface {

    void setName(String name);
    String getName();

    ChallengeRating getCR();
    void setCR(ChallengeRating CR);

    Alignment getAlignment();
    void setAlignment(Alignment alignment);

    MonsterType getType();
    void setType(MonsterType monsterType);

    String getSubtype();
    void setSubtype(String subtype);

    Size getSize();
    void setSize(Size size);

    int getSpeed();
    void setSpeed(int speed);
    void addSpecialSpeed(Speed speedType, int speed);
    void removeSpecialSpeed(Speed speedType);
    Map<Speed, Integer> getSpecialSpeeds();

    String getLanguageList();
    void setLanguageList(String languageList);

    int getProficiencyBonus();
    int getSizeMultiplier();

    void addSense(Sense sense, int range);
    void removeSense(Sense sense);
    Map<Sense, Integer> getSenses();

    void addSkill(Skill skill);
    void removeSkill(Skill skill);
    List<Skill> getSkills();

    void addSave(Ability saveAbility);
    void removeSave(Ability saveAbility);
    List<Ability> getSaves();

    void setAbilityScore(Ability ability, int newScore);
    int getAbilityScore(Ability ability);

    void setArmor(Armor armor);
    Armor getArmor();
    void setNaturalArmor(int natArmorBonus);
    int getNaturalArmor();
    void setShield(boolean hasShield);

    int getAC();
    int getACFromArmor();
    int getACFromNatArmor();
    boolean hasShield();

    void addDamageImmunity(DamageType damageType);
    void removeDamageImmunity(DamageType damageType);
    List<DamageType> getDamageImmunities();

    void addDamageResistance(DamageType damageType);
    void removeDamageResistance(DamageType damageType);
    List<DamageType> getDamageResistances();

    void addDamageVulnerability(DamageType damageType);
    void removeDamageVulnerability(DamageType damageType);
    List<DamageType> getDamageVulnerabilities();

    void addConditionImmunity(ConditionType conditionType);
    void removeConditionImmunity(ConditionType conditionType);
    List<ConditionType> getConditionImmunities();

    void addAction(String name, String description);
    void removeAction(String name);
    List<Action> getActions();

    void addAttack(String name, Weapon manufacturedWeapon, int numRiderDice, DieType riderDie, DamageType riderType, String description);
    void addAttack(String name, Weapon manufacturedWeapon, String description);
    void addAttack(String name, Ability ability, int numDice, DieType damageDie, DamageType damageType, int reach, int range, int longrange, String description);
    void removeAttack(String name);
    List<Attack> getAttacks();

    void addTrait(String name, String description, int bonusToArmorClass, int bonusToHit, double hitPointMultiplier, int damagePer3Rounds);
    void removeTrait(String name);
    List<Trait> getTraits();

    int getSaveModifier(Ability saveAbility);
    int getSkillModifier(Skill skill);
    int getModifier(Ability ability);

    ChallengeRating getCurrentCR();
    ChallengeRating getCurrentOR();
    ChallengeRating getCurrentDR();

    int getNumHitDice();
    DieType getHitDieType();

    int getHitPoints();
    int getHitPointsFromCon();


    void setNumHitDice(int numHitDice);
}
