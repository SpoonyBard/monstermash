package model;

import enums.DamageType;
import enums.DieType;

/**
 * Start of a class for a reach goal feature which was not implemented.  However, we do want to implement it in the
 * future, so the file is staying for now.
 */
public class Rider {
    private int numDice;
    private DieType damageDie;
    private DamageType damageType;

    public Rider(int numDice, DieType damageDie, DamageType damageType) {
        this.numDice = numDice;
        this.damageDie = damageDie;
        this.damageType = damageType;
    }
}
