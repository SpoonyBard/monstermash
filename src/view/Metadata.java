package view;

import enums.*;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Text;

/**
 * Class private to the view package with methods for creating the metadata tab of the main
 */
class Metadata {
    private final Tab metadataTab;
    private final ViewMain main;

    Metadata(ViewMain main) {
        this.main = main;
        this.metadataTab = createMetadataTab();
    }

    Tab getTab() {return metadataTab;}

    /**
     * Method to compose the metadata tab from name, subtitle, speed, info, and ability score blocks
     * @return metadata - metadata tab
     */
    private Tab createMetadataTab() {
        Tab metadata = new Tab("Metadata");
        VBox metadataBox = new VBox(20);

        metadataBox.getChildren().add(createNameBlock());
        metadataBox.getChildren().add(createSubtitleBlock());
        metadataBox.getChildren().add(createSpeedBlock());
        metadataBox.getChildren().add(createInfoBlock());
        metadataBox.getChildren().add(createAbilityScoreBlock());

        metadata.setContent(metadataBox);
        return metadata;
    }

    /**
     * Method to create name block of metadata tab
     * @return grid - name block
     */
    private GridPane createNameBlock() {
        GridPane grid = main.createTabContentGrid();
        Label monsterName = new Label("Name:");
        grid.add(monsterName, 0, 1);

        TextField mNameTextField = new TextField();
        mNameTextField.textProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setName(newValue));
        grid.add(mNameTextField, 1, 1);
        return grid;
    }

    /**
     * Method to create subtitle block of metadata tab
     * @return grid - subtitle block
     */
    private GridPane createSubtitleBlock() {
        GridPane grid = main.createTabContentGrid();

        Label sizeLabel = new Label("Size:");
        ComboBox<Size> sizes = makeSizeDropdown();

        Label typeLabel = new Label("Type/Tag:");
        HBox typeBox = makeTypeBox();

        Label alignmentLabel = new Label("Alignment:");
        ComboBox<Alignment> alignments = makeAlignmentDropdown();

        grid.add(sizeLabel, 0, 0);
        grid.add(sizes, 1, 0);
        grid.add(typeLabel, 0, 1);
        grid.add(typeBox, 1, 1);
        grid.add(alignmentLabel, 0, 2);
        grid.add(alignments, 1, 2);

        return grid;
    }
    /**
     * Method to create a box containing label, typeDropdown, and tagField
     * @return typeBox
     */
    private HBox makeTypeBox() {
        HBox typeBox = new HBox(10);

        ComboBox<MonsterType> types = makeTypeDropdown();

        TextField tagField = new TextField();
        tagField.setMaxWidth(ViewMain.COLUMN_WIDTH / 2);
        tagField.textProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setSubtype(newValue));

        typeBox.getChildren().add(types);
        typeBox.getChildren().add(tagField);

        return typeBox;
    }

    /**
     * Method to create a dropdown of Alignment values
     * @return alignmentDropdown
     */
    private ComboBox<Alignment> makeAlignmentDropdown() {
        ComboBox<Alignment> alignmentDropdown = new ComboBox<>();
        for (Alignment a : Alignment.values()) {
            alignmentDropdown.getItems().add(a);
        }
        alignmentDropdown.setMaxWidth(ViewMain.COLUMN_WIDTH);
        alignmentDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setAlignment(newValue));
        return alignmentDropdown;
    }

    /**
     * Method to create a dropdown of MonsterType values
     * @return monsterTypeDropdown
     */
    private ComboBox<MonsterType> makeTypeDropdown() {
        ComboBox<MonsterType> monsterTypeDropdown = new ComboBox<>();
        for (MonsterType t : MonsterType.values()) {
            monsterTypeDropdown.getItems().add(t);
        }
        monsterTypeDropdown.setMaxWidth(ViewMain.COLUMN_WIDTH / 2);
        monsterTypeDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setType(newValue));
        return monsterTypeDropdown;
    }

    /**
     * Method to create a dropdown of Size values
     * @return sizeDropdown
     */
    private ComboBox<Size> makeSizeDropdown() {
        ComboBox<Size> sizeDropdown = new ComboBox<>();
        for (Size s : Size.values()) {
            sizeDropdown.getItems().add(s);
        }
        sizeDropdown.setMaxWidth(ViewMain.COLUMN_WIDTH);
        sizeDropdown.getSelectionModel().select(Size.MEDIUM);
        sizeDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            main.controller.setSize(newValue);
            main.actions.reloadActionsTab();
        });
        return sizeDropdown;
    }

    /**
     * Method to create speed block of metadata tab
     * @return grid - speed block
     */
    private GridPane createSpeedBlock() {
        GridPane grid = main.createTabContentGrid();
        Label speed = new Label("Speed:");

        HBox speeds = new HBox(6);
        speeds.setMaxWidth(ViewMain.COLUMN_WIDTH);
        speeds.setAlignment(Pos.CENTER_LEFT);

        Spinner<Integer> speedSpinner = makeSpeedSpinner();

        Scene speedPopScene = main.makeScene(makeSpeedsPopup());
        Button speedBtn = new Button("Movement Modes");
        speedBtn.setMinWidth(153);
        speedBtn.setOnAction(event -> {
            main.popUp.setScene(speedPopScene);
            main.popUp.setTitle("Add Movement Modes");
            main.popUp.show();
        });

        Text units = new Text("ft. ");

        speeds.getChildren().addAll(speedSpinner, units, speedBtn);

        grid.add(speed, 0, 0);
        grid.add(speeds, 1, 0);

        return grid;
    }

    /**
     * Method to create a spinner of possible speeds, incrementing by 5
     * @return speedSpinner
     */
    private Spinner<Integer> makeSpeedSpinner() {
        Spinner<Integer> speedSpinner = new Spinner<>(0, 120, 30, 5);
        speedSpinner.setEditable(true);
        speedSpinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
        speedSpinner.valueProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setSpeed(newValue));
        return speedSpinner;
    }

    /**
     * Method to create popup to be displayed to select special movement speeds
     * @return speedBoxes
     */
    private VBox makeSpeedsPopup() {
        VBox speedBoxes = main.createCheckboxScene("Special Movement Speeds:");
        for (Speed s : Speed.values()) {
            Text units = new Text("ft.");
            HBox speedBox = new HBox(6);
            speedBox.setAlignment(Pos.CENTER);
            CheckBox box = new CheckBox(s.toString());
            box.setMinWidth(100);
            Spinner<Integer> spinner = new Spinner<>(0, 300, 30, 5);
            spinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
            spinner.setEditable(true);
            spinner.valueProperty().addListener((observable, oldValue, newValue) -> {
                if (box.isSelected()) {
                    main.controller.addSpeed(s, newValue);
                }
            });
            box.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    main.controller.addSpeed(s, spinner.getValue());
                } else {
                    main.controller.removeSpeed(s);
                }
            });
            speedBox.getChildren().addAll(box, spinner, units);

            speedBoxes.getChildren().add(speedBox);
        }
        Button closeSaveBtn = main.makeSimpleDoneButton();
        speedBoxes.getChildren().add(closeSaveBtn);
        return speedBoxes;
    }

    /**
     * Method to create info block of metadata tab
     * @return grid - info block
     */
    private GridPane createInfoBlock() {
        GridPane grid = main.createTabContentGrid();

        Label savesLabel = new Label("Saving Throws:");
        Scene savePopScene = main.makeScene(makeSavePopup());
        Button saveBtn = main.makePopupButton("Set Save Proficiencies", savePopScene);

        Label skillsLabel = new Label("Skills:");
        Scene skillPopScene = main.makeScene(makeSkillsPopup());
        Button skillBtn = main.makePopupButton("Set Skill Proficiencies", skillPopScene);

        Label sensesLabel = new Label("Senses:");
        Scene sensePopScene = main.makeScene(makeSensePopup());
        Button addSenses = main.makePopupButton("Add Senses", sensePopScene);

        Label languagesLabel = new Label("Languages:");
        TextField languageField = new TextField();
        languageField.textProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setLanguageList(newValue));

        grid.add(savesLabel, 0, 0);
        grid.add(saveBtn, 1, 0);
        grid.add(skillsLabel, 0, 1);
        grid.add(skillBtn, 1, 1);
        grid.add(sensesLabel, 0, 2);
        grid.add(addSenses, 1, 2);
        grid.add(languagesLabel, 0, 3);
        grid.add(languageField, 1, 3);

        return grid;
    }

    /**
     * Method to create popup to be displayed to select proficiencies with saving throws
     * @return saveBoxes
     */
    private VBox makeSavePopup() {
        VBox saveBoxes = main.createCheckboxScene("Saving Throw Proficiencies:");
        for (Ability a : Ability.values()) {
            CheckBox box = new CheckBox(a.toString());
            saveBoxes.getChildren().add(box);
            box.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue) {
                    main.controller.addSave(a);
                } else {
                    main.controller.removeSave(a);
                }
            });
        }
        Button closeSaveBtn = main.makeSimpleDoneButton();
        saveBoxes.getChildren().add(closeSaveBtn);
        return saveBoxes;
    }

    /**
     * Method to create popup to be displayed to select proficiencies with skills
     * @return skillBoxes
     */
    private VBox makeSkillsPopup() {
        VBox skillBoxes = main.createCheckboxScene("Skill Proficiencies:");
        //populate checkbox scene with the following values
        for (Skill s : Skill.values()) {
            CheckBox box = new CheckBox(s.toString());
            skillBoxes.getChildren().add(box);
            box.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue) {
                    main.controller.addSkill(s);
                } else {
                    main.controller.removeSkill(s);
                }
            });
        }
        Button closeSkillBtn = main.makeSimpleDoneButton();
        skillBoxes.getChildren().add(closeSkillBtn);
        return skillBoxes;
    }

    /**
     * Method to create popup to be displayed to select senses
     * @return senseBoxes
     */
    private VBox makeSensePopup() {
        VBox senseBoxes = main.createCheckboxScene("Senses:");
        for (Sense s : Sense.values()) {
            HBox senseBox = new HBox(6);
            senseBox.setAlignment(Pos.CENTER);
            CheckBox box = new CheckBox(s.toString());
            box.setMinWidth(120);

            Spinner<Integer> spinner = new Spinner<>(5, 500, 60, 5);
            spinner.setEditable(true);
            spinner.setMaxWidth(ViewMain.SPINNER_WIDTH);

            Text units = new Text ("ft.");

            spinner.valueProperty().addListener((observableValue, oldValue, newValue) -> {
                if (box.isSelected()) {
                    main.controller.addSense(s, newValue);
                }
            });
            box.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue) {
                    main.controller.addSense(s, spinner.getValue());
                } else {
                    main.controller.removeSense(s);
                }
            });
            senseBox.getChildren().addAll(box, spinner, units);
            senseBoxes.getChildren().add(senseBox);
        }

        Button closeSenseBtn = main.makeSimpleDoneButton();
        senseBoxes.getChildren().add(closeSenseBtn);
        return senseBoxes;
    }

    /**
     * Method to create ability score block of metadata tab
     * @return grid - ability score block
     */
    private GridPane createAbilityScoreBlock() {
        GridPane grid = main.createTabContentGrid();

        Label abilityScoreLabel = new Label("Ability Scores:");
        HBox abilityScoreBox = new HBox(10);

        for (Ability a : Ability.values()) {
            VBox box = createAbilityScoreSpinner(a);
            abilityScoreBox.getChildren().add(box);
        }

        grid.add(abilityScoreLabel, 0, 0);
        grid.add(abilityScoreBox, 0, 1, 2, 1);

        return grid;
    }

    /**
     * Method to create a spinner associated with an ability score
     * @param ability - associated ability score
     * @return box
     */
    private VBox createAbilityScoreSpinner(Ability ability) {
        VBox box = new VBox();
        Label str = new Label(ability.name());
        box.setAlignment(Pos.CENTER);

        Spinner<Integer> spin = new Spinner<>(1, 30, 10);
        spin.setEditable(true);
        spin.setMaxWidth(ViewMain.SPINNER_WIDTH);
        spin.valueProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setAbilityScore(ability, newValue));
        box.getChildren().addAll(str, spin);
        return box;
    }

}
