package view;

import enums.*;
import javafx.scene.text.Text;
import model.*;

import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * Class private to the view package with methods for creating the actions tab of the main
 */
class Actions {
    private final Tab actionsTab;
    private final ViewMain main;

    Actions(ViewMain parent) {
        this.main = parent;
        this.actionsTab = createActionsTab();
    }

    Tab getTab() {
        return actionsTab;
    }

    /**
     * Method to compose the actions tab from attack and feature blocks
     * @return actionsTab - actions tab
     */
    private Tab createActionsTab() {
        Tab actionsTab = new Tab("Actions and Traits");
        VBox actionBox = new VBox(20);

        actionBox.getChildren().add(createAttackBlock());
        actionBox.getChildren().add(createFeatureBlock());

        actionsTab.setContent(actionBox);

        return actionsTab;
    }

    /**
     * Method to refresh the Actions Tab
     */
    void reloadActionsTab() {
        main.actionsTab.setContent(createActionsTab().getContent());
    }

    /**
     * Method to create attack block of actions and traits tab
     * @return grid - attack block
     */
    private GridPane createAttackBlock() {
        GridPane grid = main.createTabContentGrid();

        Label attackLabel = new Label("Attacks:");

        Scene attackScene = createEmptyAttackScene();
        Scene customAttackScene = main.makeScene(new GridPane());
        fillEmptyCustomAttackScene(customAttackScene);

        Button attackBtn = main.makePopupButton("Add Weapon Attack", attackScene);
        Tooltip attackTip = main.makeTooltip("Give your monster an attack patterned off of a weapon.");
        Tooltip.install(attackBtn, attackTip);

        Button customAttackBtn = main.makePopupButton("Add Custom Attack", customAttackScene);
        Tooltip customAttackTip = main.makeTooltip("Specify a custom attack for your monster.  All monster attacks are based on one type of weapon damage dice, but may deviate in other respects.");
        Tooltip.install(customAttackBtn, customAttackTip);

        VBox attackListGrid = makeAttackListGrid(false);
        VBox customAttackListGrid = makeAttackListGrid(true);

        grid.add(attackLabel, 0, 0);
        grid.add(attackBtn, 0, 1);
        grid.add(customAttackBtn, 1, 1);
        grid.add(attackListGrid, 0,2);
        grid.add(customAttackListGrid, 1, 2);

        return grid;
    }

    /**
     * Method to create grid with list of attacks
     * @param custom - whether the list is of custom attacks or normal attacks
     * @return box
     */
    private VBox makeAttackListGrid(boolean custom) {
        VBox box = new VBox(5);

        for (Attack attack : main.controller.getAttacks()) {
            //if the attack is of the same type as the list that is being generated (i.e. custom or normal)
            if (attack.getCustom() == custom) {
                HBox attackBox = new HBox(5);
                Label attackName = new Label(attack.getName());
                attackName.setWrapText(true);
                attackName.setId("featureitem");

                HBox buttonBox = new HBox(3);
                Scene attackScene = main.makeScene(new GridPane());
                if (attack.getCustom()) {
                    fillCustomAttackScene(attack.getName(), attack.getCustomAttackDiceCode(), attack.getDamageType(),
                            attack.getAbility(), attack.getDescription(), attack.isMelee(), attack.isRanged(),
                            attack.getReach(), attack.getShortRange(), attack.getLongRange(), attackScene);
                } else {
                    attackScene = createAttackScene(attack.getName(), attack.getWeapon(), attack.getDescription());
                }
                Button editAttackButton = main.makePopupButton("Edit", attackScene);
                editAttackButton.setMinWidth(45);

                Button removeAttackBtn = new Button("Delete");
                removeAttackBtn.setOnAction(event -> {
                    main.controller.removeAttack(attack.getName());
                    reloadActionsTab();
                });
                removeAttackBtn.setMinWidth(62);
                buttonBox.getChildren().addAll(editAttackButton, removeAttackBtn);

                attackBox.getChildren().addAll(attackName, buttonBox);
                attackBox.setAlignment(Pos.CENTER_LEFT);
                attackName.setMinWidth(135);

                box.getChildren().add(attackBox);
            }
        }
        return box;
    }

    /**
     * Method to create features block of actions and traits tab
     * @return grid - traits features
     */
    private GridPane createFeatureBlock() {
        GridPane grid = main.createTabContentGrid();

        Label featureLabel = new Label("Features:");
        Button actionBtn = makeAddActionBtn();
        Button traitBtn = makeAddTraitBtn();

        Label actionLabel = new Label();
        VBox actionBox = makeActionGrid();
        if (!main.controller.getActions().isEmpty()) {
            actionLabel.setText("Actions:");
        }

        Label traitLabel = new Label();
        VBox traitBox = makeTraitGrid();
        if (!main.controller.getTraits().isEmpty()) {
            traitLabel.setText("Traits:");
        }

        grid.add(featureLabel, 0, 0);
        grid.add(actionBtn, 0, 1);
        grid.add(traitBtn, 1, 1);
        grid.add(actionLabel, 0, 2);
        grid.add(traitLabel, 1, 2);
        grid.add(actionBox, 0, 3);
        grid.add(traitBox, 1, 3);

        return grid;
    }

    /**
     * Method to create grid containing list of traits
     * @return box
     */
    private VBox makeTraitGrid() {
        VBox box = new VBox(5);

        for (Trait trait : main.controller.getTraits()) {
            HBox traitBox = new HBox(5);
            Label traitName = new Label(trait.getName());
            traitName.setWrapText(true);
            traitName.setId("featureitem");

            HBox buttonBox = new HBox(3);
            Scene traitScene = createTraitScene(trait.getName(), trait.getDescription(), trait.getBonusToArmorClass(),
                    trait.getBonusToHit(), trait.getHitPointMultiplier(), trait.getDPR());
            Button editTraitBtn = main.makePopupButton("Edit", traitScene);
            editTraitBtn.setMinWidth(45);

            Button removeTraitBtn = new Button("Delete");
            removeTraitBtn.setOnAction(event -> {
                main.controller.removeTrait(trait.getName());
                reloadActionsTab();
            });
            removeTraitBtn.setMinWidth(62);
            buttonBox.getChildren().addAll(editTraitBtn, removeTraitBtn);

            traitBox.getChildren().addAll(traitName, buttonBox);
            traitBox.setAlignment(Pos.CENTER_LEFT);
            traitName.setMinWidth(135);

            box.getChildren().add(traitBox);
        }
        return box;
    }

    /**
     * Method to create grid containing list of actions
     * @return box
     */
    private VBox makeActionGrid() {
        VBox box = new VBox(5);

        for (Action action : main.controller.getActions()) {
            HBox actionBox = new HBox(5);
            Label actionName = new Label(action.getName());
            actionName.setWrapText(true);
            actionName.setId("featureitem");

            HBox buttonBox = new HBox(3);
            Scene actionScene = createActionScene(action.getName(), action.getDescription());
            Button editActionBtn = main.makePopupButton("Edit", actionScene);
            editActionBtn.setMinWidth(45);

            Button removeActionBtn = new Button("Delete");
            removeActionBtn.setOnAction(event -> {
                main.controller.removeAction(action.getName());
                reloadActionsTab();
            });
            removeActionBtn.setMinWidth(62);
            buttonBox.getChildren().addAll(editActionBtn, removeActionBtn);

            actionBox.getChildren().addAll(actionName, buttonBox);
            actionBox.setAlignment(Pos.CENTER_LEFT);
            actionName.setMinWidth(135);

            box.getChildren().add(actionBox);
        }
        return box;
    }

    /**
     * Method to create button that makes a popup for adding an action
     * @return actionBtn
     */
    private Button makeAddActionBtn() {
        Button actionBtn = new Button("Add Action");
        actionBtn.setMaxWidth(ViewMain.COLUMN_WIDTH);
        actionBtn.setOnAction(event -> {
            Scene featurePopScene = createEmptyActionScene();
            main.popUp.setScene(featurePopScene);
            main.popUp.setTitle("Add Action");
            main.popUp.show();
        });
        Tooltip actionTip = main.makeTooltip("Describe an action for your monster that does not have any effect on its stats.  Future updates may include multiattack and area of effect actions.");
        Tooltip.install(actionBtn, actionTip);
        return actionBtn;
    }

    /**
     * Method to create button that makes a popup for adding a trait
     * @return traitBtn
     */
    private Button makeAddTraitBtn() {
        Button traitBtn = new Button("Add Trait");
        traitBtn.setMaxWidth(ViewMain.COLUMN_WIDTH);
        traitBtn.setOnAction(event -> {
            Scene featurePopScene = createEmptyTraitScene();
            main.popUp.setScene(featurePopScene);
            main.popUp.setTitle("Add Trait");
            main.popUp.show();
        });
        Tooltip traitTip = main.makeTooltip("Give your monster a trait that may or may not effect its stats.");
        Tooltip.install(traitBtn, traitTip);
        return traitBtn;
    }

    /**
     * Method to create a popup for user action creation by loading default values
     * @return actionScene
     */
    private Scene createEmptyActionScene() {
        return createActionScene("", "");
    }

    /**
     * Method to create a popup for user action-creation by loading previous user input
     * @param name - action name
     * @param description - action description
     * @return - actionScene - action scene
     */
    private Scene createActionScene(String name, String description) {
        GridPane actionGrid = new GridPane();
        actionGrid.setMaxWidth(ViewMain.COLUMN_WIDTH * 2);

        actionGrid.setVgap(10);
        actionGrid.setPadding(new Insets(10));

        Label actionNameLabel = new Label("Name:");
        TextField actionNameTextField = new TextField(name);

        Label actionDescriptionLabel = new Label("Description:");
        TextArea actionDescriptionTextArea = new TextArea(description);
        actionDescriptionTextArea.setMaxHeight(100);
        actionDescriptionTextArea.setWrapText(true);

        Button closeBtn = new Button("Save");
        closeBtn.setOnAction(event -> {
            if ((actionNameTextField.getText() != null) && (!actionNameTextField.getText().equals(""))){
                main.controller.removeAction(name);
                main.controller.addAction(actionNameTextField.getText(), actionDescriptionTextArea.getText());
            }
            reloadActionsTab();
            main.popUp.hide();
        });
        closeBtn.setDefaultButton(true);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(closeBtn);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);

        actionGrid.add(actionNameLabel, 0, 0);
        actionGrid.add(actionNameTextField, 1, 0);
        actionGrid.add(actionDescriptionLabel, 0, 2);
        actionGrid.add(actionDescriptionTextArea, 0, 3, 2, 1);
        actionGrid.add(buttonBox, 0, 4, 2, 1);

        return main.makeScene(actionGrid);
    }

    /**
     * Method to create a popup for user trait-creation by loading default values
     * @return traitScene
     */
    private Scene createEmptyTraitScene() {
        return createTraitScene("", "", 0, 0, 1, 0);
    }

    /**
     * Method to create a popup for user trait-creation by loading previous user input
     * @param name - trait name
     * @param description - trait description
     * @param bonusToArmorClass - bonus to armor class
     * @param bonusToHit - bonus to hit
     * @param hitPointMultiplier - hit point multiplier
     * @param DPR - damage per round
     * @return - traitScene - trait scene
     */
    private Scene createTraitScene(String name, String description, int bonusToArmorClass, int bonusToHit, double hitPointMultiplier, int DPR) {
        GridPane traitGrid = new GridPane();
        traitGrid.setMaxWidth(ViewMain.COLUMN_WIDTH * 2);

        traitGrid.setVgap(10);
        traitGrid.setPadding(new Insets(10));

        Label traitNameLabel = new Label("Name:");
        TextField traitNameTextField = new TextField(name);

        Label traitDescriptionLabel = new Label("Description:");
        TextArea traitDescriptionTextArea = new TextArea(description);
        traitDescriptionTextArea.setMaxHeight(100);
        traitDescriptionTextArea.setWrapText(true);

        /*
         * Next section creates the instructions as well as the spinners for setting the trait's attributes.
         */
        Text instructions = new Text("Use the following fields to change your monster's stats if and only if the trait's effects cannot be entered elsewhere in the stat block.\nFor example, if a trait gives a monster a constant AC bonus, you should change that by increasing its natural armor bonus.\nHowever, if the trait only increased the monster's AC in certain circumstances, this would be an appropriate place.\nAs a final note, advantage works out to a +4 or +5 bonus and the reverse for disadvantage.");
        instructions.setWrappingWidth(400);
        HBox instructionBox = new HBox();
        instructionBox.getChildren().add(instructions);

        Label ACBonusLabel = new Label("Bonus to Armor Class: ");
        Spinner<Integer> ACBonusSpinner = makeIntegerSpinner(bonusToArmorClass, 10);
        Tooltip ACBonus = main.makeTooltip("If the trait makes the monster easier or harder to hit, estimate the AC modifier here.");
        Tooltip.install(ACBonusLabel, ACBonus);
        Tooltip.install(ACBonusSpinner, ACBonus);

        Label hitBonusLabel = new Label("Bonus to Hit: ");
        Spinner<Integer> hitBonusSpinner = makeIntegerSpinner(bonusToHit,30);
        Tooltip HitBonus = main.makeTooltip("If the trait makes the monster more or less likely to hit, estimate the effect on its to-hit bonus here.");
        Tooltip.install(hitBonusLabel, HitBonus);
        Tooltip.install(hitBonusSpinner, HitBonus);

        Label hitPointMultiplierLabel = new Label("Hit Point Multiplier: ");
        Spinner<Double> hitPointMultiplierSpinner = new Spinner<>(0.1, 5.0, hitPointMultiplier, 0.1);
        hitPointMultiplierSpinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
        hitPointMultiplierSpinner.setEditable(true);
        Tooltip HPmultiplier = main.makeTooltip("If the trait affects the amount of damage the monster can take, set its approximate HP multiplier here.");
        Tooltip.install(hitPointMultiplierLabel, HPmultiplier);
        Tooltip.install(hitPointMultiplierSpinner, HPmultiplier);

        Label damagePerRoundLabel = new Label("Additional Damage Per Round: ");
        Spinner<Integer> damagePerRoundSpinner = makeIntegerSpinner(DPR, 1000);
        Tooltip DPRtip = main.makeTooltip("If the trait adds to the monster's damage per round, set its expected DPR increase here.");
        Tooltip.install(damagePerRoundLabel, DPRtip);
        Tooltip.install(damagePerRoundSpinner, DPRtip);



        /*
         * This section closes the pop-up and saves its values.
         */
        Button closeBtn = new Button("Save");
        closeBtn.setOnAction(event -> {
            Integer ACbonus = ACBonusSpinner.getValue();
            Integer hitBonus = hitBonusSpinner.getValue();
            Double HPMultiplier = hitPointMultiplierSpinner.getValue();
            Integer damagePerRound = damagePerRoundSpinner.getValue();
            if ((traitNameTextField.getText() != null) && (!traitNameTextField.getText().equals(""))){
                main.controller.removeTrait(name);
                main.controller.addTrait(traitNameTextField.getText(), traitDescriptionTextArea.getText(), ACbonus, hitBonus, HPMultiplier, damagePerRound);
            }
            reloadActionsTab();
            main.popUp.hide();
        });
        closeBtn.setDefaultButton(true);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(closeBtn);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);

        traitGrid.add(traitNameLabel, 0, 0);
        traitGrid.add(traitNameTextField, 1, 0);
        traitGrid.add(traitDescriptionLabel, 0, 2);
        traitGrid.add(traitDescriptionTextArea, 0, 3, 2, 1);
        traitGrid.add(instructionBox, 0, 5, 2, 1);
        traitGrid.add(ACBonusLabel, 0, 6);
        traitGrid.add(ACBonusSpinner, 1, 6);
        traitGrid.add(hitBonusLabel, 0, 7);
        traitGrid.add(hitBonusSpinner, 1, 7);
        traitGrid.add(hitPointMultiplierLabel, 0, 8);
        traitGrid.add(hitPointMultiplierSpinner, 1, 8);
        traitGrid.add(damagePerRoundLabel, 0, 9);
        traitGrid.add(damagePerRoundSpinner, 1, 9);
        traitGrid.add(buttonBox, 0, 11, 2, 1);

        return main.makeScene(traitGrid);
    }

    /**
     * Method to create a spinner with consistent formatting given currentValue, min, and max
     * @param currentValue - current value
     * @param max - spinner maximum
     * @return spinner
     */
    private Spinner<Integer> makeIntegerSpinner(int currentValue, int max) {
        Spinner<Integer> spinner = new Spinner<>(0, max, currentValue);
        spinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
        spinner.setEditable(true);
        return spinner;
    }

    /**
     * Method to create a popup for user attack-creation by loading default values
     * @return attackGrid - attack grid
     */
    private Scene createEmptyAttackScene() {
        return createAttackScene("", Weapon.CLUB, "");
    }

    /**
     * Method to create a popup for user attack-creation by loading previous user input
     * @param name - attack name
     * @param weapon - weapon used
     * @param description - attack description
     * @return attackGrid - attack grid
     */
    private Scene createAttackScene(String name, Weapon weapon, String description) {
        GridPane attackGrid = new GridPane();

        attackGrid.setVgap(10);
        attackGrid.setHgap(10);
        attackGrid.setMaxWidth(550);
        attackGrid.setPadding(new Insets(10));

        Label attackNameLabel = new Label("Name:");
        TextField attackNameTextField = new TextField(name);

        Label dieMultiplier = new Label(main.controller.getStringAttackMultiplier());
        ComboBox<Weapon> weaponDropdown = makeWeaponDropdown();
        weaponDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> attackNameTextField.setText(newValue.getStringName()));

        weaponDropdown.getSelectionModel().select(weapon);

        Label attackDescriptionLabel = new Label("Description:");
        TextArea attackDescriptionTextArea = new TextArea(description);
        attackDescriptionTextArea.setMaxSize(325, 100);
        attackDescriptionTextArea.setWrapText(true);

        Button closeAttackBtn = new Button("Save");
        closeAttackBtn.setOnAction(event -> {
            if ((attackNameTextField.getText() != null) && (!attackNameTextField.getText().equals("")) && (weaponDropdown.getValue() != null)){
                main.controller.removeAttack(name);
                main.controller.addAttack(attackNameTextField.getText(), weaponDropdown.getValue(), attackDescriptionTextArea.getText());
            }
            reloadActionsTab();
            main.popUp.hide();
        });
        closeAttackBtn.setDefaultButton(true);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(closeAttackBtn);

        attackGrid.add(attackNameLabel, 0, 0);
        attackGrid.add(attackNameTextField, 1, 0);

        attackGrid.add(dieMultiplier, 0, 1);
        GridPane.setHalignment(dieMultiplier, HPos.RIGHT);
        attackGrid.add(weaponDropdown, 1, 1, 3, 1);

        attackGrid.add(attackDescriptionLabel, 0, 2);
        attackGrid.add(attackDescriptionTextArea, 1, 2);

        attackGrid.add(buttonBox, 0, 3, 2, 1);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        return main.makeScene(attackGrid);
    }

    /**
     * Method to create a dropdown of DamageType values
     * @return damageDropdown
     */
    private ComboBox<DamageType> makeDamageDropdown() {
        ComboBox<DamageType> damageDropdown = new ComboBox<>();
        for (DamageType a : DamageType.values()) {
            damageDropdown.getItems().add(a);
        }
        return damageDropdown;
    }

    /**
     * Method to create a dropdown of AttackDiceCode values
     * @return diceCodeDropdown
     */
    private ComboBox<AttackDiceCode> makeDiceCodeDropdown() {
        ComboBox<AttackDiceCode> diceCodeDropdown = new ComboBox<>();
        for (AttackDiceCode a : AttackDiceCode.values()) {
            diceCodeDropdown.getItems().add(a);
        }
        return diceCodeDropdown;
    }

    /**
     * Method to create a dropdown of Ability values
     * @return abilityDropdown
     */
    private ComboBox<Ability> makeAbilityDropdown() {
        ComboBox<Ability> abilityDropdown = new ComboBox<>();
        for (Ability a : Ability.values()) {
            abilityDropdown.getItems().add(a);
        }
        return abilityDropdown;
    }

    /**
     * Method to create a dropdown of Weapon values
     * @return weaponDropdown
     */
    private ComboBox<Weapon> makeWeaponDropdown() {
        ComboBox<Weapon> weaponDropdown = new ComboBox<>();
        for (Weapon a : Weapon.values()) {
            weaponDropdown.getItems().add(a);
        }
        return weaponDropdown;
    }

    /**
     * Method to fill custom attack scene with default values
     * @param customAttackScene - scene to be filled with information
     */
    private void fillEmptyCustomAttackScene(Scene customAttackScene) {
        fillCustomAttackScene("", AttackDiceCode.ONED4, DamageType.BLUDGEONING, Ability.STR, "", true,
                false, 5, 0, 0, customAttackScene);
    }

    /**
     * Method to fill custom attack scene dynamically based on melee and ranged booleans
     * @param name - custom attack name
     * @param attackDiceCode - custom attack dice code
     * @param damageType - custom attack damage type
     * @param ability - ability used for custom attack
     * @param description - description of custom attack
     * @param melee - whether the attack is melee
     * @param ranged - whether the attack is ranged
     * @param reach - the reach of the attack
     * @param shortRange - the short range of the attack
     * @param longRange - the long range of the attack
     * @param attackScene - the scene to be filled with information
     */
    private void fillCustomAttackScene(String name, AttackDiceCode attackDiceCode, DamageType damageType, Ability ability,
                                       String description, boolean melee, boolean ranged, int reach, int shortRange, int longRange, Scene attackScene) {
        GridPane attackGrid = new GridPane();

        attackGrid.setVgap(10);
        attackGrid.setHgap(10);
        attackGrid.setPadding(new Insets(10));
        attackGrid.setMaxWidth(530);

        Label attackNameLabel = new Label("Name:");
        TextField attackNameTextField = new TextField(name);

        Label dieMultiplier = new Label(main.controller.getStringAttackMultiplier());
        dieMultiplier.setAlignment(Pos.CENTER);
        ComboBox<AttackDiceCode> damageDiceCodeDropdown = makeDiceCodeDropdown();
        damageDiceCodeDropdown.getSelectionModel().select(attackDiceCode);
        ComboBox<DamageType> damageTypeDropdown = makeDamageDropdown();
        damageTypeDropdown.getSelectionModel().select(damageType);

        Label abilityDropdownLabel = new Label("Ability used:");
        ComboBox<Ability> abilityDropdown = makeAbilityDropdown();
        abilityDropdown.getSelectionModel().select(ability);

        HBox rangeRadioButtons = new HBox();
        final ToggleGroup rangeGroup = new ToggleGroup();

        RadioButton meleeButton = new RadioButton("Melee");
        RadioButton rangedButton = new RadioButton("Ranged");
        RadioButton meleeRangedButton = new RadioButton("Thrown");

        meleeButton.setToggleGroup(rangeGroup);
        rangedButton.setToggleGroup(rangeGroup);
        meleeRangedButton.setToggleGroup(rangeGroup);

        rangeRadioButtons.getChildren().addAll(meleeButton, rangedButton, meleeRangedButton);
        rangeRadioButtons.setSpacing(5);
        rangeRadioButtons.setAlignment(Pos.CENTER_LEFT);


        if (melee && !ranged) {
            ((RadioButton)rangeRadioButtons.getChildren().get(0)).setSelected(true);
        } else if (ranged && !melee) {
            ((RadioButton)rangeRadioButtons.getChildren().get(1)).setSelected(true);
        } else {
            ((RadioButton)rangeRadioButtons.getChildren().get(2)).setSelected(true);
        }

        VBox rangeSpinners = new VBox(6);

        HBox reachBox = new HBox(3);
        Label reachLabel = new Label("Reach:");
        Spinner<Integer> reachSpinner = new Spinner<>(0, 30, reach, 5);
        reachSpinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
        reachBox.getChildren().addAll(reachLabel, reachSpinner);
        reachBox.setAlignment(Pos.CENTER_LEFT);

        HBox rangeBox = new HBox(3);
        Label shortRangeLabel = new Label("Short Range:");
        Spinner<Integer> shortRangeSpinner = new Spinner<>(0, 200, shortRange, 5);
        shortRangeSpinner.setMaxWidth(ViewMain.SPINNER_WIDTH);

        Label longRangeLabel = new Label("Long Range:");
        Spinner<Integer> longRangeSpinner = new Spinner<>(0, 800, longRange, 5);
        longRangeSpinner.setMaxWidth(ViewMain.SPINNER_WIDTH);
        rangeBox.getChildren().addAll(shortRangeLabel, shortRangeSpinner, longRangeLabel, longRangeSpinner);
        rangeBox.setAlignment(Pos.CENTER_LEFT);

        if (melee) {
            rangeSpinners.getChildren().add(reachBox);
        }
        if (ranged) {
            rangeSpinners.getChildren().addAll(rangeBox);
        }

        rangeSpinners.setMinWidth(200);

        Label attackDescriptionLabel = new Label("Description:");
        TextArea attackDescriptionTextArea = new TextArea(description);
        attackDescriptionTextArea.setMaxHeight(100);
        attackDescriptionTextArea.setWrapText(true);

        Button closeAttackBtn = new Button("Save");
        closeAttackBtn.setOnAction(event -> {
            if ((attackNameTextField.getText() != null) && (!attackNameTextField.getText().equals(""))){
                main.controller.removeAttack(name);
                main.controller.addCustomAttack(attackNameTextField.getText(), abilityDropdown.getValue(),
                        damageDiceCodeDropdown.getValue(), damageTypeDropdown.getValue(), reachSpinner.getValue(),
                        shortRangeSpinner.getValue(), longRangeSpinner.getValue(), attackDescriptionTextArea.getText());
            }
            reloadActionsTab();
            main.popUp.hide();
        });
        closeAttackBtn.setDefaultButton(true);
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(closeAttackBtn);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);

        rangeGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == meleeButton) {
                fillCustomAttackScene(attackNameTextField.getText(), damageDiceCodeDropdown.getValue(),
                        damageTypeDropdown.getValue(), abilityDropdown.getValue(), attackDescriptionTextArea.getText(), true, false,
                        reachSpinner.getValue(), 0, 0, attackScene);
            } else if (newValue == rangedButton) {
                fillCustomAttackScene(attackNameTextField.getText(), damageDiceCodeDropdown.getValue(),
                        damageTypeDropdown.getValue(), abilityDropdown.getValue(), attackDescriptionTextArea.getText(), false, true,
                        0, shortRangeSpinner.getValue(), longRangeSpinner.getValue(), attackScene);
            } else {
                fillCustomAttackScene(attackNameTextField.getText(), damageDiceCodeDropdown.getValue(),
                        damageTypeDropdown.getValue(), abilityDropdown.getValue(), attackDescriptionTextArea.getText(), true, true,
                        reachSpinner.getValue(), shortRangeSpinner.getValue(), longRangeSpinner.getValue(), attackScene);
            }
        });

        attackGrid.add(attackNameLabel, 0, 0);
        attackGrid.add(attackNameTextField, 1, 0, 2, 1);

        attackGrid.add(dieMultiplier, 0, 1);
        GridPane.setHalignment(dieMultiplier, HPos.RIGHT);
        attackGrid.add(damageDiceCodeDropdown, 1, 1);
        attackGrid.add(damageTypeDropdown, 2, 1);

        attackGrid.add(abilityDropdownLabel, 0, 2);
        attackGrid.add(abilityDropdown,1,2);

        attackGrid.add(rangeRadioButtons, 1, 3, 2, 1);

        attackGrid.add(rangeSpinners, 1, 4, 2, 1);

        attackGrid.add(attackDescriptionLabel, 0, 5);
        attackGrid.add(attackDescriptionTextArea, 1, 5, 2, 1);

        attackGrid.add(buttonBox, 2, 6);

        attackScene.setRoot(attackGrid);
    }
}

