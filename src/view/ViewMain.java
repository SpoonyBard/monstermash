package view;

import controller.ControllerInterface;
import enums.ChallengeRating;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.web.*;
import javafx.scene.SnapshotParameters;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ViewMain {
    static final int COLUMN_WIDTH = 250;
    static final int SPINNER_WIDTH = 80;
    final ControllerInterface controller;
    private BorderPane root;

    //pieces of the root requiring updating
    public Stage popUp;
    private final Defense defense;
    final Actions actions;
    Tab actionsTab;
    private WebView statblock;
    private WebEngine webEngine;

    public ViewMain(Stage primaryStage, ControllerInterface controller) {
        this.controller = controller;
        this.defense = new Defense(this);
        this.actions = new Actions(this);
        startUp(primaryStage);
    }

    /**
     * Method called by the controller whenever it receives new information
     * @param htmlStr - dynamically built html string
     */
    public void setHTML(String htmlStr) {
        webEngine.loadContent(htmlStr);
    }

    /**
     * Method called by the controller to get an image of the statblock
     * @return snapshot of statblock
     */
    public WritableImage makeExportImage() {
        return statblock.snapshot(new SnapshotParameters(), null);
    }

    /**
     * Method called by the controller to refresh the information in the CR information block
     */
    public void reloadCRBlock() {
        root.setTop(createHeading());
    }


    /**
     * Method to compose the main scene from heading, stat block, and tabs panes,
     * as well as set up the popup functionality
     * @param primaryStage - Stage for the main scene
     */
    private void startUp(Stage primaryStage) {

        primaryStage.setTitle("Monster Mash");
        popUp = new Stage();
        popUp.setAlwaysOnTop(true);

        root = new BorderPane();
        root.setTop(createHeading());
        root.setRight(createStatBlock());
        root.setCenter(createTabs());

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(ViewMain.class.getResource("GUI.css").toExternalForm());
        primaryStage.show();
        root.requestFocus();
    }

    /*
     * Method to create the heading pane
     * @return headBox - heading pane
     */
    private VBox createHeading() {
        VBox headBox = new VBox();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        menuBar.getMenus().addAll(fileMenu);

        MenuItem newFile = new MenuItem("New");
        newFile.setOnAction(event -> controller.newMonster());
        MenuItem exportHTML = new MenuItem("Export as HTML");
        exportHTML.setOnAction(event -> controller.exportAsHtml());
        MenuItem exportPNG = new MenuItem("Export as PNG");
        exportPNG.setOnAction(event -> controller.exportAsPng());
        fileMenu.getItems().addAll(newFile, exportHTML, exportPNG);

        HBox headingContent = new HBox(15);
        headingContent.setPadding(new Insets(3, 0, 0, 6));
        VBox headingTextBox = new VBox();
        headingTextBox.setAlignment(Pos.BOTTOM_LEFT);
        headingTextBox.setMinWidth(COLUMN_WIDTH * 2 + 20);
        Text basicHeading = new Text("Monster Mash: 5e D&D Monster Builder");
        basicHeading.setId("heading");
        headingTextBox.getChildren().add(basicHeading);

        HBox CRoptions = makeCRBlock();
        CRoptions.setPadding(new Insets(0, 0, 5, 0));

        headingContent.getChildren().addAll(headingTextBox, CRoptions);

        headBox.getChildren().addAll(menuBar, headingContent);
        return headBox;
    }

    /**
     * Method to create the CR information block
     * @return CRBlock
     */
    private HBox makeCRBlock() {
        HBox CRBlock = new HBox(10);
        CRBlock.setAlignment(Pos.CENTER);
        CRBlock.setId("cr-options");

        Label targetCRLabel = new Label("Target CR: ");
        ComboBox<ChallengeRating> challengeRatingDropdown = makeChallengeRatingDropdown();
        HBox targetCRBox = makeCRBlockBox(targetCRLabel, challengeRatingDropdown);

        Tooltip targetCRTip = new Tooltip("Set the desired challenge rating for your monster here.  The values to the right display the monster's current challenge rating based on its stats.");
        targetCRTip.setWrapText(true);
        targetCRTip.setMaxWidth(200);
        Tooltip.install(targetCRBox, targetCRTip);
        Tooltip.install(targetCRLabel, targetCRTip);
        Tooltip.install(challengeRatingDropdown, targetCRTip);

        Label currentCRLabel = new Label("Current CR: ");
        Text currentCRText = new Text(controller.getCurrentCR());
        if (!controller.getCurrentCR().equals(controller.getCR().CRdisplay())) {
            currentCRText.setFill(Color.RED);
        }
        Tooltip CR = makeTooltip("Challenge Rating of the current monster. Challenge Rating is the average of the Offensive and Defensive Ratings.");
        Tooltip.install(currentCRText, CR);
        Tooltip.install(currentCRLabel, CR);
        HBox crBox = makeCRBlockBox(currentCRLabel, currentCRText);

        Label currentORLabel = new Label("Offense: ");
        Text currentORText = new Text(controller.getCurrentOR());
        Tooltip OR = makeTooltip("Offensive Rating of the current monster. Offensive Rating is affected by average damage per round and Attack Bonus.");
        Tooltip.install(currentORText, OR);
        Tooltip.install(currentORLabel, OR);
        HBox orBox = makeCRBlockBox(currentORLabel, currentORText);

        Label currentDRLabel = new Label("Defense: ");
        Text currentDRText = new Text(controller.getCurrentDR());
        Tooltip DR = makeTooltip("Defensive Rating of the current monster. Defensive Rating is affected by Armor Class, Hit Points, Immunities, Vulnerabilities, and Resistances.");
        Tooltip.install(currentDRText, DR);
        Tooltip.install(currentDRLabel, DR);
        HBox drBox = makeCRBlockBox(currentDRLabel, currentDRText);

        CRBlock.getChildren().addAll(targetCRBox, crBox, orBox, drBox);

        return CRBlock;
    }



    /**
     * Method to create an individual label and value pair for display
     * @param label - label for display
     * @param display - value for display
     * @return box - box containing pair
     */
    private HBox makeCRBlockBox(Label label, Node display) {
        HBox box = new HBox();
        box.setAlignment(Pos.CENTER_LEFT);
        box.getChildren().addAll(label, display);

        return box;
    }

    /**
     * Method to create a dropdown of ChallengeRating values
     * @return challengeRatingDropdown
     */
    private ComboBox<ChallengeRating> makeChallengeRatingDropdown() {
        ComboBox<ChallengeRating> challengeRatingDropdown = new ComboBox<>();
        for (ChallengeRating cr : ChallengeRating.values()) {
            challengeRatingDropdown.getItems().add(cr);
        }
        challengeRatingDropdown.setMaxWidth(60);
        challengeRatingDropdown.getSelectionModel().select(controller.getCR());
        challengeRatingDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> controller.setCR(newValue));
        return challengeRatingDropdown;
    }

    /**
     * Method to create the stat block pane which is a WebView because
     * it displays the Monster StatBlock using dynamic HTML
     * @return statblock - Monster StatBlock
     */
    private WebView createStatBlock() {
        statblock = new WebView();
        statblock.setBlendMode(BlendMode.DARKEN);
        webEngine = statblock.getEngine();
        statblock.setMaxWidth(450);
        webEngine.setUserStyleSheetLocation(getClass().getResource("statblock.css").toString());
        return statblock;
    }

    /**
     * Method to compose the tabs pane from metadata, defense, and actions tabs
     * @return tabs - tabs pane
     */
    private TabPane createTabs() {
        TabPane tabs = new TabPane();
        tabs.setMinWidth(COLUMN_WIDTH * 2);
        tabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab metadataTab = new Metadata(this).getTab();
        Tab defenseTab = defense.getTab();
        actionsTab = actions.getTab();

        tabs.getTabs().add(metadataTab);
        tabs.getTabs().add(defenseTab);
        tabs.getTabs().add(actionsTab);
        return tabs;
    }

    /**
     * Method to create a grid pane with formatting. Since multiple methods call this method,
     * the grid panes created are consistently formatted.
     * @return grid - grid pane
     */
    GridPane createTabContentGrid() {
        GridPane grid = new GridPane();
        grid.setVgap(5);
        grid.setHgap(5);
        grid.setPadding(new Insets(5, 15, 0, 15));
        grid.getColumnConstraints().add(new ColumnConstraints(ViewMain.COLUMN_WIDTH));
        grid.getColumnConstraints().add(new ColumnConstraints(ViewMain.COLUMN_WIDTH));
        return grid;
    }

    /**
     * Method to create tooltip. Since multiple methods call this method,
     * the tooltips will be consistently formatted
     * @param text - text in tooltip
     * @return tooltip
     */
    Tooltip makeTooltip(String text) {
        Tooltip tooltip = new Tooltip(text);
        tooltip.setWrapText(true);
        tooltip.setMaxWidth(200);
        return tooltip;
    }

    /**
     * Method to create button with associated popup functionality
     * @param text - text on button
     * @param scene - scene shown in popup when button is clicked
     * @return button - popup button
     */
    Button makePopupButton(String text, Scene scene) {
        Button button = new Button(text);
        button.setMaxWidth(ViewMain.COLUMN_WIDTH);
        button.setOnAction(event -> {
            popUp.setScene(scene);
            popUp.setTitle(text);
            popUp.show();
        });
        return button;
    }

    /**
     * Method to create a scene for use in a popup
     * @param box - what to add to the scene
     * @return scene - created scene
     */
    Scene makeScene(Region box) {
        Scene scene = new Scene(box);
        scene.getStylesheets().add(ViewMain.class.getResource("GUI.css").toExternalForm());
        return scene;
    }

    /**
     * Method to create a simple button labeled "Done" that hides the popup
     * @return doneButton - simple done button
     */
    Button makeSimpleDoneButton() {
        Button doneButton = new Button("Done");
        doneButton.setOnAction(event -> popUp.hide());
        doneButton.setDefaultButton(true);
        return doneButton;
    }

    /**
     * Method to create a popup containing checkboxes
     * @param text - label for the popup
     * @return checkBoxScene - check box scene
     */
    VBox createCheckboxScene(String text) {
        VBox checkBoxScene = new VBox(10);
        checkBoxScene.setPadding(new Insets(10));
        Label checkBoxLabel = new Label(text);
        checkBoxScene.getChildren().add(checkBoxLabel);
        return checkBoxScene;
    }
}