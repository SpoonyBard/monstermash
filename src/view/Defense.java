package view;

import enums.*;

import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Class private to the view package with methods for creating the defense tab of the main
 */
class Defense {
    private final Tab defenseTab;
    private final ViewMain main;

    Defense(ViewMain parent) {
        this.main = parent;
        this.defenseTab = createDefenseTab();
    }

    Tab getTab() {
        return defenseTab;
    }

    /**
     * Method to compose the defense tab from armor and special defense blocks
     * @return defense - defense tab
     */
    private Tab createDefenseTab() {
        Tab defenseTab = new Tab("Defense");
        VBox defenseBox = new VBox();

        defenseBox.getChildren().add(createArmorBlock());
        defenseBox.getChildren().add(createSpecialDefBlock());

        defenseTab.setContent(defenseBox);
        return defenseTab;
    }

    /**
     * Method to create armor block of defense tab
     * @return grid - armor block
     */
    private GridPane createArmorBlock() {
        GridPane grid = main.createTabContentGrid();

        Label armorType = new Label("Armor:");
        ComboBox<Armor> armorTypes = makeArmorDropdown();
        Tooltip armorTypeTip = main.makeTooltip("Equip your monster with armor from the Player's Handbook.  This does not stack with natural armor.");
        Tooltip.install(armorType, armorTypeTip);
        Tooltip.install(armorTypes, armorTypeTip);

        Label shield = new Label("Shield:");
        CheckBox shieldBox = makeShieldCheckbox();
        Tooltip shieldTip = main.makeTooltip("Give your monster a shield.  Shields increase AC by 2, stacking with everything, but keep in mind that most monsters need to use an arm to hold a shield.");
        Tooltip.install(shield, shieldTip);
        Tooltip.install(shieldBox, shieldTip);


        Label natArmorLabel = new Label("Natural Armor Bonus:");
        Spinner<Integer> natArmorSpinner = new Spinner<>(0, 15, 0);
        natArmorSpinner.setEditable(true);
        natArmorSpinner.setMaxWidth(60);
        natArmorSpinner.valueProperty().addListener((observable, oldValue, newValue) -> main.controller.setNaturalArmor(newValue));

        Tooltip naturalArmorTip = main.makeTooltip("Natural Armor Bonus stacks with Dexterity, but does not stack with Manufactured Armor. The higher of the two acts as the monster's AC.");
        Tooltip.install(natArmorLabel, naturalArmorTip);
        Tooltip.install(natArmorSpinner, naturalArmorTip);

        Label numHitDiceLabel = new Label("Number of Hit Dice:");

        Spinner<Integer> numHitDiceSpinner = new Spinner<>(1, 200, 0);
        numHitDiceSpinner.setEditable(true);
        numHitDiceSpinner.setMaxWidth(60);
        numHitDiceSpinner.valueProperty().addListener((observable, oldValue, newValue) -> main.controller.setNumHitDice(newValue));

        Tooltip hitDiceTip = main.makeTooltip("Specify your monster's number of hit dice.  Hit die type is controlled by monster size.");
        Tooltip.install(numHitDiceLabel, hitDiceTip);
        Tooltip.install(numHitDiceSpinner, hitDiceTip);


        grid.add(armorType, 0, 0);
        grid.add(armorTypes, 1, 0);
        grid.add(shield, 0, 1);
        grid.add(shieldBox, 1, 1);
        grid.add(natArmorLabel, 0, 2);
        grid.add(natArmorSpinner, 1, 2);
        grid.add(numHitDiceLabel, 0, 3);
        grid.add(numHitDiceSpinner, 1, 3);

        return grid;
    }

    /**
     * Method to create checkbox for monster's use of a shield
     * @return shieldBox
     */
    private CheckBox makeShieldCheckbox() {
        CheckBox shieldBox = new CheckBox();
        shieldBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (shieldBox.isSelected()) {
                main.controller.setShield(true);
            } else {
                main.controller.setShield(false);
            }
        });
        return shieldBox;
    }

    /**
     * Mehod to create a dropdown of Armor values
     * @return armorDropdown
     */
    private ComboBox<Armor> makeArmorDropdown() {
        ComboBox<Armor> armorDropdown = new ComboBox<>();
        for (Armor a : Armor.values()) {
            armorDropdown.getItems().add(a);
        }
        armorDropdown.setMaxWidth(ViewMain.COLUMN_WIDTH);
        armorDropdown.getSelectionModel().selectFirst();

        armorDropdown.valueProperty().addListener((observableValue, oldValue, newValue) -> main.controller.setArmor(newValue));
        return armorDropdown;
    }

    /**
     * Method to create special defense block of defense tab
     * @return grid - special defense block
     */
    private GridPane createSpecialDefBlock() {
        GridPane grid = main.createTabContentGrid();

        Label specialDefLabel = new Label("Immunities and Resistances:");

        Tooltip specialDefTip = main.makeTooltip("Give your monster resistances, immunities, or vulnerabilities to conditions and damage types.");
        Tooltip.install(specialDefLabel, specialDefTip);

        TabPane specialDefTabs = new TabPane();
        specialDefTabs.setId("defensetabs");
        specialDefTabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab damageTab = makeDamagesTab();
        Tab condImmunitiesTab = makeCondImmunitiesTab();

        specialDefTabs.getTabs().add(damageTab);
        specialDefTabs.getTabs().add(condImmunitiesTab);

        grid.add(specialDefLabel, 0, 3, 2, 1);
        grid.add(specialDefTabs, 0, 4, 2, 1);

        return grid;
    }

    /**
     * Method to create condition immunities tab
     * @return condImmunitiesTab
     */
    private Tab makeCondImmunitiesTab() {
        Tab condImmunitiesTab = new Tab("Condition Immunities");
        VBox condImmunityBoxes = new VBox(10);
        condImmunityBoxes.setPadding(new Insets(10, 0, 10, 10));
        for (ConditionType t : ConditionType.values()) {
            CheckBox box = new CheckBox(t.toString());
            condImmunityBoxes.getChildren().add(box);
            box.selectedProperty().addListener((observableValue, oldValue, newValue) -> {
                if (newValue) {
                    main.controller.addCondImmunity(t);
                } else {
                    main.controller.removeCondImmunity(t);
                }
            });
        }
        condImmunitiesTab.setContent(condImmunityBoxes);
        return condImmunitiesTab;
    }

    /**
     * Method to create tab with radiobuttons for each type of damage
     * @return damageTab
     */
    private Tab makeDamagesTab() {
        Tab damageTab = new Tab("Damage Types");
        GridPane damages = new GridPane();

        for (int i = 0; i < 5; i++) {
            damages.getColumnConstraints().add(new ColumnConstraints(100));
        }

        damages.setVgap(10);
        damages.setPadding(new Insets(10,0,20,10));

        Text resistantLabel = new Text ("Resistant");
        Text vulnerableLabel = new Text ("Vulnerable");
        Text immuneLabel = new Text ("Immune");
        Text noneLabel = new Text ("None ");

        damages.add(resistantLabel, 1, 0);
        GridPane.setHalignment(resistantLabel, HPos.CENTER);

        damages.add(vulnerableLabel, 2, 0);
        GridPane.setHalignment(vulnerableLabel, HPos.CENTER);

        damages.add(immuneLabel, 3, 0);
        GridPane.setHalignment(immuneLabel, HPos.CENTER);

        damages.add(noneLabel, 4, 0);
        GridPane.setHalignment(noneLabel, HPos.CENTER);


        int counter = 1;
        for (DamageType t : DamageType.values()) {
            Text damageType = new Text(t.toString());
            ToggleGroup damageGroup = new ToggleGroup();

            RadioButton resistant = new RadioButton();
            resistant.setToggleGroup(damageGroup);

            RadioButton vulnerable = new RadioButton();
            vulnerable.setToggleGroup(damageGroup);

            RadioButton immune = new RadioButton();
            immune.setToggleGroup(damageGroup);

            RadioButton none = new RadioButton();
            none.setToggleGroup(damageGroup);
            none.setSelected(true);

            damageGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
                if (resistant.isSelected()) {
                    main.controller.addResistance(t);
                } else if (vulnerable.isSelected()) {
                    main.controller.addDmgVulnerability(t);
                } else if (immune.isSelected()) {
                    main.controller.addDmgImmunity(t);
                } else if (none.isSelected()){
                    main.controller.removeDmgImmunity(t);
                    main.controller.removeDmgVulnerability(t);
                    main.controller.removeResistance(t);
                }
            });

            damages.add(damageType, 0, counter);

            damages.add(resistant, 1, counter);
            GridPane.setHalignment(resistant, HPos.CENTER);

            damages.add(vulnerable, 2, counter);
            GridPane.setHalignment(vulnerable, HPos.CENTER);

            damages.add(immune, 3, counter);
            GridPane.setHalignment(immune, HPos.CENTER);

            damages.add(none, 4, counter);
            GridPane.setHalignment(none, HPos.CENTER);

            counter++;
        }
        damageTab.setContent(damages);
        return damageTab;
    }
}
