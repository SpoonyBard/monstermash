package controller;

import enums.*;
import model.*;
import java.util.List;

/**
 * Interface for the controller. Offers public-facing methods for the
 * view to use.
 */
public interface ControllerInterface {
    /**
     * Creates new Monster object and connects it to the view, overwriting previous monster.
     */
    void newMonster();

    /**
     * Uses ImageIO to export statblock as .png file.
     */
    void exportAsPng();

    /**
     * Uses FileIO to export statblock as .html file.
     */
    void exportAsHtml();

    /**
     * Method called every time new data is passed to the Monster.
     * Updates statblock html and CR display box.
     */
    void updateView();

    /**
     * Methods for setting and getting the challenge rating of a monster.
     */
    void setCR(ChallengeRating CR);
    ChallengeRating getCR();

    /**
     * Methods for getting the current defensive, offensive, and current challenge rating.
     */
    String getCurrentDR();
    String getCurrentOR();
    String getCurrentCR();

    /**
     * Methods for setting metadata information that will update in the view through the
     * updateView() method.
     */
    void setName(String name);
    void setAlignment(Alignment alignment);
    void setType(MonsterType monsterType);
    void setSubtype(String subtype);
    void setSize(Size size);
    void setSpeed(int speed);
    void setLanguageList(String languageList);
    void setAbilityScore(Ability score, int newValue);

    /**
     * Methods for setting defense information that will update in the view through the
     * updateView() method.
     */
    void setArmor(Armor armor);
    void setNaturalArmor(int natArmorBonus);
    void setShield(boolean hasShield);
    void setNumHitDice(int numHitDice);

    /**
     * Methods for adding and deleting monster senses.
     */
    void addSense(Sense sense, int range);
    void removeSense(Sense sense);

    /**
     * Methods for adding and deleting monster damage resistances.
     */
    void addResistance(DamageType damageType);
    void removeResistance(DamageType damageType);

    /**
     * Methods for adding and deleting monster damage immunities.
     */
    void addDmgImmunity(DamageType damageType);
    void removeDmgImmunity(DamageType damageType);

    /**
     * Methods for adding and deleting monster damage vulnerabilities.
     */
    void addDmgVulnerability(DamageType t);
    void removeDmgVulnerability(DamageType t);

    /**
     * Methods for adding and deleting monster condition immunities.
     */
    void addCondImmunity(ConditionType conditionType);
    void removeCondImmunity(ConditionType conditionType);

    /**
     * Methods for adding and deleting monster skills.
     */
    void addSkill(Skill skill);
    void removeSkill(Skill skill);

    /**
     * Methods for adding and deleting monster saving throw proficiencies.
     */
    void addSave(Ability saveAbility);
    void removeSave(Ability saveAbility);

    /**
     * Methods for adding and deleting monster speeds.
     */
    void addSpeed(Speed speedType, int speed);
    void removeSpeed(Speed speedType);

    /**
     * Methods for adding and deleting monster traits, as well as loading traits as a list.
     */
    void addTrait(String traitName, String traitDescription, int ACbonus, int HitBonus, double HitPointMultiplier, int DamagePer3Rounds);
    void removeTrait(String traitName);
    List<Trait> getTraits();

    /**
     * Methods for adding and deleting monster actions, as well as loading actions as a list.
     */
    void addAction(String actionName, String actionDescription);
    void removeAction(String actionName);
    List<Action> getActions();

    /**
     * Methods for adding and deleting monster attacks and custom attacks, as well as loading attacks as a list.
     */
    void addAttack(String name, Weapon manufacturedWeapon, String description);
    void addCustomAttack(String name, Ability ability, AttackDiceCode attackDiceCode, DamageType damageType, int reach, int shortRange, int longRange, String description);
    void removeAttack(String name);
    List<Attack> getAttacks();

    /**
     * Method for getting attack multiplier for display.
     * @return attackMultiplier
     */
    String getStringAttackMultiplier();

}
