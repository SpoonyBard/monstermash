package controller;

import view.ViewMain;
import enums.*;
import model.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StatsController implements ControllerInterface {
    private MonsterInterface monster;
    private ViewMain view;
    private final Stage stage;

    public StatsController(Stage primaryStage) {
        monster = new Monster();
        this.stage = primaryStage;
        view = new ViewMain(primaryStage, this);
        updateView();
    }

    /**
     * Creates new Monster object and connects it to the view.
     */
    public void newMonster() {
        monster = new Monster();
        view = new ViewMain(this.stage, this);
        updateView();
    }

    /**
     * Uses ImageIO to export statblock as .png file.
     */
    public void exportAsPng() {
        WritableImage image = view.makeExportImage();

        String name = monster.getName().replace(" ", "") + ".png";

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export as .png");
        fileChooser.setInitialFileName(name);
        File file = fileChooser.showSaveDialog(view.popUp);

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses FileIO to export statblock as .html file.
     */
    public void exportAsHtml() {
        String name = monster.getName().replace(" ", "") + ".html";
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export as .html");
        fileChooser.setInitialFileName(name);
        File file = fileChooser.showSaveDialog(view.popUp);

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(file));
            out.write(getExportHTMLHeading());
            out.write(getUpdatedHTML());
            out.write(getExportHTMLEnding());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method called every time new data is passed to the Monster
     * Updates statblock html and CR display box.
     */
    public void updateView() {
        view.setHTML(getUpdatedHTML());
        view.reloadCRBlock();
    }

    /**
     * Methods for setting and getting the challenge rating of a monster.
     */
    public void setCR(ChallengeRating CR) {
        monster.setCR(CR);
        updateView();
    }

    public ChallengeRating getCR() {
        return monster.getCR();
    }

    /**
     * Methods for getting the current defensive, offensive, and current challenge rating.
     */
    public String getCurrentDR() {
        return monster.getCurrentDR().CRdisplay();
    }

    public String getCurrentOR() {
        return monster.getCurrentOR().CRdisplay();
    }

    public String getCurrentCR() {
        ChallengeRating currentCR = monster.getCurrentCR();
        return currentCR.CRdisplay();
    }

    /**
     * Methods for setting metadata information that will update in the view through the
     * updateView() method.
     */
    public void setName(String name) {
        monster.setName(name);
        updateView();
    }

    public void setAlignment(Alignment alignment) {
        monster.setAlignment(alignment);
        updateView();
    }

    public void setType(MonsterType monsterType) {
        monster.setType(monsterType);
        updateView();
    }

    public void setSubtype(String subtype) {
        monster.setSubtype(subtype);
        updateView();
    }

    public void setSize(Size size) {
        monster.setSize(size);
        updateView();
    }

    public void setSpeed(int speed) {
        monster.setSpeed(speed);
        updateView();
    }

    public void setLanguageList(String languageList) {
        monster.setLanguageList(languageList);
        updateView();
    }

    public void setAbilityScore(Ability score, int newValue) {
        monster.setAbilityScore(score, newValue);
        updateView();
    }

    /**
     * Methods for setting defense information that will update in the view through the
     * updateView() method.
     */
    public void setArmor(Armor armor) {
        monster.setArmor(armor);
        updateView();
    }

    public void setNaturalArmor(int natArmorBonus) {
        monster.setNaturalArmor(natArmorBonus);
        updateView();
    }

    public void setShield(boolean hasShield) {
        monster.setShield(hasShield);
        updateView();
    }

    public void setNumHitDice(int numHitDice) {
        monster.setNumHitDice(numHitDice);
        updateView();
    }

    /**
     * Methods for adding and deleting monster senses.
     */
    public void addSense(Sense sense, int range) {
        monster.addSense(sense, range);
        updateView();
    }

    public void removeSense(Sense sense) {
        monster.removeSense(sense);
        updateView();
    }

    /**
     * Methods for adding and deleting monster damage resistances.
     */
    public void addResistance(DamageType damageType) {
        monster.removeDamageImmunity(damageType);
        monster.removeDamageVulnerability(damageType);
        monster.addDamageResistance(damageType);
        updateView();
    }

    public void removeResistance(DamageType damageType) {
        monster.removeDamageResistance(damageType);
        updateView();
    }

    /**
     * Methods for adding and deleting monster damage immunities.
     */
    public void addDmgImmunity(DamageType damageType) {
        monster.removeDamageResistance(damageType);
        monster.removeDamageVulnerability(damageType);
        monster.addDamageImmunity(damageType);
        updateView();
    }

    public void removeDmgImmunity(DamageType damageType) {
        monster.removeDamageImmunity(damageType);
        updateView();
    }

    /**
     * Methods for adding and deleting monster damage vulnerabilities.
     */
    public void addDmgVulnerability(DamageType damageType) {
        monster.removeDamageResistance(damageType);
        monster.removeDamageImmunity(damageType);
        monster.addDamageVulnerability(damageType);
        updateView();
    }

    public void removeDmgVulnerability(DamageType damageType) {
        monster.removeDamageVulnerability(damageType);
        updateView();
    }

    /**
     * Methods for adding and deleting monster condition immunities.
     */
    public void addCondImmunity(ConditionType conditionType) {
        monster.addConditionImmunity(conditionType);
        updateView();
    }

    public void removeCondImmunity(ConditionType conditionType) {
        monster.removeConditionImmunity(conditionType);
        updateView();
    }

    /**
     * Methods for adding and deleting monster skills.
     */
    public void addSkill(Skill skill) {
        monster.addSkill(skill);
        updateView();
    }

    public void removeSkill(Skill skill) {
        monster.removeSkill(skill);
        updateView();
    }

    /**
     * Methods for adding and deleting monster saving throw proficiencies.
     */
    public void addSave(Ability saveAbility) {
        monster.addSave(saveAbility);
        updateView();
    }

    public void removeSave(Ability saveAbility) {
        monster.removeSave(saveAbility);
        updateView();
    }

    /**
     * Methods for adding and deleting monster speeds.
     */
    public void addSpeed(Speed speedType, int speed) {
        monster.addSpecialSpeed(speedType, speed);
        updateView();
    }

    public void removeSpeed(Speed speedType) {
        monster.removeSpecialSpeed(speedType);
        updateView();
    }

    /**
     * Methods for adding and deleting monster traits, as well as loading traits as a list.
     */
    public void addTrait(String traitName, String traitDescription, int ACbonus, int HitBonus, double HitPointMultiplier, int DamagePer3Rounds) {
        monster.addTrait(traitName, traitDescription, ACbonus, HitBonus, HitPointMultiplier, DamagePer3Rounds);
        updateView();
    }

    public void removeTrait(String traitName) {
        monster.removeTrait(traitName);
        updateView();
    }

    public List<Trait> getTraits() {
        return monster.getTraits();
    }

    /**
     * Methods for adding and deleting monster actions, as well as loading actions as a list.
     */
    public void addAction(String actionName, String actionDescription) {
        monster.addAction(actionName, actionDescription);
        updateView();
    }

    public void removeAction(String actionName) {
        monster.removeAction(actionName);
        updateView();
    }

    public List<Action> getActions() {
        return monster.getActions();
    }

    /**
     * Methods for adding and deleting monster attacks and custom attacks, as well as loading attacks as a list.
     */
    public void addAttack(String name, Weapon manufacturedWeapon, String description) {
        monster.addAttack(name, manufacturedWeapon, description);
        updateView();
    }

    public void addCustomAttack(String name, Ability ability, AttackDiceCode damageCode, DamageType damageType,  int reach, int shortRange, int longRange, String description) {
        monster.addAttack(name, ability, damageCode.getNumDice(), damageCode.getDieType(), damageType, reach, shortRange, longRange, description);
        updateView();
    }

    public void removeAttack(String name) {
        monster.removeAttack(name);
        updateView();
    }

    /**
     * Method for getting attack multiplier for display.
     * @return attackMultiplier
     */
    public List<Attack> getAttacks() {
        return monster.getAttacks();
    }

    public String getStringAttackMultiplier() {
        return String.valueOf(monster.getSizeMultiplier()) + " × ";
    }

    /**
     * Method to get modifier for a given ability as a string
     * @param ability - ability modifier
     * @return modifier
     */
    private String getStringModifier(Ability ability) {
        int modifier = monster.getModifier(ability);
        if (modifier >= 0) {
            return "+" + modifier;
        } else {
            return String.valueOf(modifier);
        }
    }

    /**
     * Method to get modifier for a save ability as a string
     * @param saveAbility - save ability modifier
     * @return modifier
     */
    private String getStringSaveModifier(Ability saveAbility) {
        int modifier = monster.getSaveModifier(saveAbility);
        if (modifier >= 0) {
            return "+" + modifier;
        } else {
            return String.valueOf(modifier);
        }
    }

    /**
     * Method to get a skill modifier as a string
     * @param skill - skill modifier
     * @return modifier
     */
    private String getStringSkillModifier(Skill skill) {
        int modifier = monster.getSkillModifier(skill);
        if (modifier >= 0) {
            return "+" + modifier;
        } else {
            return String.valueOf(modifier);
        }
    }

    /**
     * Method to get a dynamically built html string that includes
     * all new information pulled from the monster for display
     * @return htmlStr
     */
    private String getUpdatedHTML() {
        StringBuffer htmlStr = new StringBuffer();

        // create heading containing monster name, size, type, tag/subtype, and alignment
        htmlStr.append("<div class'stat-block'>\n<div class='bar'></div>\n<div id=\"content-wrap\">\n<div id='creature-heading'>\n<h1>");
        htmlStr.append(monster.getName());
        htmlStr.append("</h1>\n<h2>");
        if (monster.getSize() != null) {
            htmlStr.append(monster.getSize());
            htmlStr.append(" ");
        }
        if (monster.getType() != null) {
            htmlStr.append(monster.getType().toString().toLowerCase());
        }
        if (!monster.getSubtype().equals("")) {
            htmlStr.append(" (");
            htmlStr.append(monster.getSubtype().toLowerCase());
            htmlStr.append(")");
        }
        if (monster.getAlignment() != null && monster.getType() != null) {
            htmlStr.append(", ");
        }
        if (monster.getAlignment() != null) {
            htmlStr.append(monster.getAlignment().toString().toLowerCase());
        }

        // create first block containing AC, HP, HD, and speeds
        htmlStr.append("</h2>\n</div>\n<svg height='5' width='400'>\n<polyline points='0,0 400,2.5 0,5'/>\n</svg>\n<div id='top-stats'>\n<div class='property-line'>\n<h4>Armor Class</h4>\n<p>");
        htmlStr.append(monster.getAC());
        if (monster.getArmor() != Armor.NONE || monster.getNaturalArmor() != 0) {
            htmlStr.append(" (");
            if (monster.getACFromArmor() >= monster.getACFromNatArmor()) {
                htmlStr.append(monster.getArmor().toString().toLowerCase());
            } else {
                htmlStr.append("natural armor");
            }
            if (monster.hasShield()) {
                htmlStr.append(", shield");
            }
            htmlStr.append(")");
        } else if (monster.hasShield()) {
            htmlStr.append(" (shield)");
        }
        htmlStr.append("</p>\n</div>\n<div class='property-line'>\n<h4>Hit Points</h4>\n<p>");
        htmlStr.append(monster.getHitPoints());
        htmlStr.append(" (");
        htmlStr.append(monster.getNumHitDice());
        htmlStr.append(monster.getHitDieType());
        if (monster.getHitPointsFromCon() > 0) {
            htmlStr.append(" + ");
            htmlStr.append(monster.getHitPointsFromCon());
        } else if (monster.getHitPointsFromCon() < 0) {
            htmlStr.append(" - ");
            htmlStr.append(Math.abs(monster.getHitPointsFromCon()));
        }
        htmlStr.append(")</p>\n</div>\n<div class='property-line'>\n<h4>Speed</h4>\n<p>");
        htmlStr.append(monster.getSpeed());
        htmlStr.append(" ft.");
        if (!monster.getSpecialSpeeds().isEmpty()) {
            htmlStr.append(", ");
            for (Map.Entry<Speed, Integer> pair : monster.getSpecialSpeeds().entrySet()) {
                htmlStr.append(pair.getKey().name().toLowerCase());
                htmlStr.append(" ");
                htmlStr.append(pair.getValue());
                htmlStr.append(" ft., ");
            }
            //deletes the last ", " to end the list
            htmlStr.delete(htmlStr.length() - 2, htmlStr.length() - 1);
        }

        // create ability score block
        htmlStr.append("</p>\n</div>\n<svg height='5' width='400'>\n<polyline points='0,0 400,2.5 0,5'/>\n</svg>\n<table><tr>");
        htmlStr.append("<th>STR</th><th>DEX</th><th>CON</th><th>INT</th><th>WIS</th><th>CHA</th></tr><tr>");

        htmlStr.append("<td id='str'>");
        htmlStr.append(monster.getAbilityScore(Ability.STR));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.STR));
        htmlStr.append(")</td>");

        htmlStr.append("<td id='dex'>");
        htmlStr.append(monster.getAbilityScore(Ability.DEX));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.DEX));
        htmlStr.append(")</td>");

        htmlStr.append("<td id='con'>");
        htmlStr.append(monster.getAbilityScore(Ability.CON));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.CON));
        htmlStr.append(")</td>");

        htmlStr.append("<td id='int'>");
        htmlStr.append(monster.getAbilityScore(Ability.INT));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.INT));
        htmlStr.append(")</td>");

        htmlStr.append("<td id='wis'>");
        htmlStr.append(monster.getAbilityScore(Ability.WIS));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.WIS));
        htmlStr.append(")</td>");

        htmlStr.append("<td id='cha'>");
        htmlStr.append(monster.getAbilityScore(Ability.CHA));
        htmlStr.append(" (");
        htmlStr.append(getStringModifier(Ability.CHA));
        htmlStr.append(")</td>");

        // create second block containing saves, skills, special defenses, senses, and languages
        htmlStr.append("</tr></table><svg height='5' width='400'>\n<polyline points='0,0 400,2.5 0,5'/>\n</svg>\n");
        if (!monster.getSaves().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Saving Throws</h4>\n<p>");
            for (Ability saveAbility : monster.getSaves()) {
                htmlStr.append(saveAbility);
                htmlStr.append(" ");
                htmlStr.append(getStringSaveModifier(saveAbility));
                if (monster.getSaves().indexOf(saveAbility) != monster.getSaves().size() - 1) {
                    htmlStr.append(", ");
                }
            }
            htmlStr.append("</p></div>");
        }
        if (!monster.getSkills().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Skills</h4>\n<p>");
            for (Skill skill : monster.getSkills()) {
                htmlStr.append(skill);
                htmlStr.append(" ");
                htmlStr.append(getStringSkillModifier(skill));
                if (monster.getSkills().indexOf(skill) != monster.getSkills().size() - 1) {
                    htmlStr.append(", ");
                }
            }
            htmlStr.append("</p></div>") ;
        }
        if (!monster.getDamageResistances().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Resistances</h4>\n<p>");
            htmlStr.append(monster.getDamageResistances().stream().map(Object::toString).collect(Collectors.joining(", ")).toLowerCase());
            htmlStr.append("</p></div>");
        }
        if (!monster.getDamageImmunities().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Damage Immunities</h4>\n<p>");
            htmlStr.append(monster.getDamageImmunities().stream().map(Object::toString).collect(Collectors.joining(", ")).toLowerCase());
            htmlStr.append("</p>\n</div>\n");
        }
        if (!monster.getDamageVulnerabilities().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Damage Vulnerabilities</h4>\n<p>");
            htmlStr.append(monster.getDamageVulnerabilities().stream().map(Object::toString).collect(Collectors.joining(", ")).toLowerCase());
            htmlStr.append("</p>\n</div>\n");
        }
        if (!monster.getConditionImmunities().isEmpty()) {
            htmlStr.append("<div class='property-line'>\n<h4>Condition Immunities</h4>\n<p>");
            htmlStr.append(monster.getConditionImmunities().stream().map(Object::toString).collect(Collectors.joining(", ")).toLowerCase());
            htmlStr.append("</p>\n</div>\n");
        }
        htmlStr.append("<div class='property-line'>\n<h4>Senses</h4>\n<p>");
        if (!monster.getSenses().isEmpty()) {
            for (Map.Entry<Sense, Integer> pair : monster.getSenses().entrySet()) {
                htmlStr.append(pair.getKey().name().toLowerCase());
                htmlStr.append(" ");
                htmlStr.append(pair.getValue());
                htmlStr.append(" ft., ");
            }
        }
        htmlStr.append(" passive Perception ");
        htmlStr.append(10 + monster.getSkillModifier(Skill.PERCEPTION));
        htmlStr.append("</p>\n</div>\n<div class='property-line'>\n<h4>Languages</h4>\n<p>");
        if (!monster.getLanguageList().equals("")) {
            htmlStr.append(monster.getLanguageList());
        } else {
            htmlStr.append("—");
        }
        htmlStr.append("</p>\n</div>\n");
        if (monster.getCR() != null) {
            htmlStr.append("<div class='property-line'>\n<h4>Challenge</h4>\n<p>");
            htmlStr.append(monster.getCR());
            htmlStr.append(" (");
            htmlStr.append(monster.getCR().XP());
            htmlStr.append(" XP)</p>\n</div>\n");
        }
        // create block for traits and actions
        htmlStr.append(" <svg height='5' width='400'>\n<polyline points='0,0 400,2.5 0,5'/>\n</svg>\n</div>\n<div class='property-block'></div>");
        if (!monster.getTraits().isEmpty()) {
            for (Trait trait : monster.getTraits()) {
                htmlStr.append("  <div class='property-block'>\n <h4>");
                htmlStr.append(trait.getName());
                htmlStr.append(".</h4>\n<p>");
                htmlStr.append(trait.getDescription());
                htmlStr.append("</p>\n</div>");
            }
        }
        if (!monster.getActions().isEmpty() || (!monster.getAttacks().isEmpty())) {
            htmlStr.append("<h3>Actions</h3>\n");
            if (!monster.getAttacks().isEmpty()) {
                for (Attack attack : monster.getAttacks()) {
                    htmlStr.append("<div class='property-block'>\n<h4>");
                    htmlStr.append(attack.getName());
                    htmlStr.append(".</h4>\n<em>");
                    htmlStr.append(attack.getRangeString());
                    htmlStr.append(" Weapon Attack:</em><p> ");
                            if (attack.getToHitBonus() >= 0) {
                                htmlStr.append("+");
                                htmlStr.append(attack.getToHitBonus());
                            } else {
                                htmlStr.append(attack.getToHitBonus());
                            }
                            htmlStr.append(" to hit");
                    if (attack.getRangeString().contains("Melee")) {
                        htmlStr.append(", reach ");
                        htmlStr.append(attack.getReach());
                        htmlStr.append(" ft.");
                    }
                    if (attack.getRangeString().contains("Ranged")) {
                        htmlStr.append(", range ");
                        htmlStr.append(attack.getShortRange());
                        htmlStr.append(" ft./");
                        htmlStr.append(attack.getLongRange());
                        htmlStr.append("ft.");
                    }
                    htmlStr.append("<em> Hit:</em> ");
                    htmlStr.append(attack.getDPR());
                    htmlStr.append(" (");
                    htmlStr.append(attack.getDiceCode());
                    if (attack.getAbilityMod() > 0) {
                        htmlStr.append(" + ");
                        htmlStr.append(attack.getAbilityMod());
                    } else if (attack.getAbilityMod() < 0 ) {
                        htmlStr.append(" - ");
                        htmlStr.append((Math.abs(attack.getAbilityMod())));
                    }
                    htmlStr.append(") ");
                    htmlStr.append(String.valueOf(attack.getDamageType()).toLowerCase());
                    htmlStr.append(" damage. ");
                    htmlStr.append(attack.getDescription());
                    htmlStr.append("</p>\n</div>\n");
                }
            }
            if (!monster.getActions().isEmpty()) {
                for (Action action : monster.getActions()) {
                    htmlStr.append("<div class='property-block'>\n<h4>");
                    htmlStr.append(action.getName());
                    htmlStr.append(".</h4>\n<p>");
                    htmlStr.append(action.getDescription());
                    htmlStr.append("</p>\n</div>\n");
                }
            }
        }
        htmlStr.append("</div><div class='bar'></div>\n");

        return String.valueOf(htmlStr);
    }

    /**
     * Closes open portions of html so that the html of the
     * statblock can be exported
     * @return String ending - closing html
     */
    private String getExportHTMLEnding() {
        return "</body></html>";
    }

    /**
     * Creates heading for html document so that the html
     * of the statblock can be exported
     * @return String heading - html document heading
     */
    private String getExportHTMLHeading() {
        StringBuffer heading = new StringBuffer();
        heading.append("<!DOCTYPE html>\n<html>\n<head>\n<style>");

        String css = "";
        try {
            InputStream input = StatsController.class.getResourceAsStream("statblock.css");

            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;
            while ((line = reader.readLine()) != null) {
                css = css + line;
            }
            input.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        heading.append(css);

        heading.append("</style>\n</head>\n<body>");
        return String.valueOf(heading);
    }
}

