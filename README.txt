Monster Mash: 5e Monster Stat Block Creator


Abstract

Monster Mash is a tool designed for game masters of 5th Edition Dungeons and Dragons 
meant to help them design monsters and adversaries to send against their players.  It 
fills this role using two powerful abilities:  First, it allows users to create a 
formatted stat block for their monsters which can be exported either as nicely-styled
HTML or as a PNG.  Second, it calculates the stat block's challenge rating - that is, 
its expected difficulty level as an adversary - in real time, allowing users to adjust
their creation's stats to match it to their players' level.


Using Monster Mash

Using Monster Mash is meant to be straightforward.  There are three tabs - "Metadata", 
"Defense", and "Actions and Traits" - each of which contain forms that can be filled out
to give a monster certain attributes.  These are displayed on the right hand of the 
screen in a preview of what the finished stat block will look like.  Users can specify
a desired challenge rating and see current challenge rating information above the stat 
block.  Finally, options to export a stat block or begin a new stat block are found 
under "File" in the upper-left corner.

To run Monster Mash, run MonsterMash.java.


Known Bugs

The only major bug that we have discovered is that exporting the stat block as a PNG 
does not adjust for the length of the stat block.  If the stat block is long enough to 
require a scroll bar, the bottom will be cut off in the PNG.

Additionally, we made a conscious choice not to have damage resistances and immunities 
affect a monster's defensive rating, because the system outlined in the Dungeon Master's
Guide is highly subjective and would have been difficult to implement.  For now, this is
something to be fixed in the future, when we actually use this app for our own purposes.